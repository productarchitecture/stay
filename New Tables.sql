SET NAMES 'utf8';

# ##################################
# QUOREAPI
# ##################################
USE quoreapi;

CREATE TABLE IF NOT EXISTS api_signers_map_user_reward (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  api_signers_map_user_id INT(11) UNSIGNED NOT NULL,
  api_signers_map_reward_id INT(11) UNSIGNED NOT NULL,
  guest_reward_number VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB;
ALTER TABLE api_signers_map_user_reward 
  ADD INDEX FK_api_signers_map_user_reward_api_signers_map_reward_id(api_signers_map_reward_id);


CREATE TABLE IF NOT EXISTS api_signers_map_user (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  api_signers_id INT(11) NOT NULL,
  signer_guest_id VARCHAR(255) DEFAULT NULL,
  signer_guest_first_name VARCHAR(255) DEFAULT NULL,
  signer_guest_last_name VARCHAR(255) DEFAULT NULL,
  signer_guest_mobile_phone VARCHAR(100) DEFAULT NULL,
  signer_guest_email VARCHAR(255) DEFAULT NULL,
  last_update DATETIME DEFAULT CURRENT_TIMESTAMP,
  user_id INT(11) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB;
ALTER TABLE api_signers_map_user 
  ADD INDEX `idx.api_signers_map_user.api_signers_id.signer_guest_id`(api_signers_id, signer_guest_id);
ALTER TABLE api_signers_map_user 
  ADD UNIQUE INDEX UK_api_signers_map_user_api_signers_id(api_signers_id);


CREATE TABLE IF NOT EXISTS api_signers_map_reward (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  api_signers_id INT(11) DEFAULT NULL,
  signer_reward_id VARCHAR(255) BINARY DEFAULT NULL,
  signer_reward_name VARCHAR(255) BINARY DEFAULT NULL,
  quorehotel_reward_program_id INT(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB;
ALTER TABLE api_signers_map_reward 
  ADD INDEX `fk.api_signers_map_reward.api_signers_id.signer_reward_id`(api_signers_id, signer_reward_id);


CREATE TABLE IF NOT EXISTS api_signers_map_property_rate_area_type (
  api_signers_map_property_rate_id INT(11) UNSIGNED NOT NULL,
  api_signers_map_property_area_type_id INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (api_signers_map_property_rate_id, api_signers_map_property_area_type_id)
)
ENGINE = INNODB;


CREATE TABLE IF NOT EXISTS api_signers_map_property_rate (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  api_signers_map_property_id INT(11) UNSIGNED NOT NULL,
  signer_rate_id VARCHAR(25) NOT NULL,
  signer_rate_code VARCHAR(25) DEFAULT NULL,
  start_date DATE DEFAULT NULL,
  end_date DATE DEFAULT NULL,
  is_active TINYINT(1) UNSIGNED DEFAULT 1,
  quorehotel_currency_code_id INT(11) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB;
ALTER TABLE api_signers_map_property_rate 
  ADD INDEX `idx.api_signers_map_property_rate.property_id.rate_id.date`(api_signers_map_property_id, signer_rate_id, end_date);


CREATE TABLE IF NOT EXISTS api_signers_map_property_area_type (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  api_signers_map_property_id INT(11) UNSIGNED DEFAULT NULL,
  signer_room_type_id VARCHAR(50) NOT NULL,
  signer_room_type_code VARCHAR(50) DEFAULT NULL,
  signer_room_type_name VARCHAR(50) DEFAULT NULL,
  quorehotel_area_alt_name_id INT(10) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB;
ALTER TABLE api_signers_map_property_area_type 
  ADD INDEX `idx.api_signers_map_property_area_type.area_alt_name_id`(quorehotel_area_alt_name_id);
ALTER TABLE api_signers_map_property_area_type 
  ADD INDEX `idx.api_signers_map_property_area_type.property_id.room_type_id`(api_signers_map_property_id, signer_room_type_id);
ALTER TABLE api_signers_map_property_area_type 
  ADD INDEX `idx.api_signers_map_property_area_type.room_type_name`(signer_room_type_name);


CREATE TABLE IF NOT EXISTS api_signers_map_property_area (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  api_signers_map_property_id INT(11) UNSIGNED DEFAULT NULL,
  signer_room_id VARCHAR(50) NOT NULL,
  signer_room_name VARCHAR(50) DEFAULT NULL,
  quorehotel_area_base_id INT(11) UNSIGNED DEFAULT NULL,
  api_signers_map_property_area_type_id INT(11) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB;
ALTER TABLE api_signers_map_property_area 
  ADD INDEX `idx.api_signers_map_property_area.property_id.room_id`(api_signers_map_property_id, signer_room_id);
ALTER TABLE api_signers_map_property_area 
  ADD INDEX `idx.api_signers_map_property_area.quorehotel.area_base`(quorehotel_area_base_id);
ALTER TABLE api_signers_map_property_area 
  ADD INDEX `idx.api_signers_map_property_area.roomNumber`(signer_room_name);
ALTER TABLE api_signers_map_property_area 
  ADD INDEX `idx.api_signers_map_property_area.signer_room_type_name`(api_signers_map_property_area_type_id);


CREATE TABLE IF NOT EXISTS api_signers_map_property (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  api_signers_id INT(11) NOT NULL,
  quorehotel_property_id INT(11) DEFAULT NULL,
  signer_hotel_id VARCHAR(255) DEFAULT NULL,
  signer_other_id VARCHAR(255) DEFAULT NULL,
  signer_hotel_code VARCHAR(25) DEFAULT NULL,
  signer_hotel_name VARCHAR(255) DEFAULT NULL,
  signer_hotel_chain VARCHAR(255) DEFAULT NULL,
  signer_hotel_brand VARCHAR(255) DEFAULT NULL,
  inserted_on DATETIME DEFAULT CURRENT_TIMESTAMP,
  deactivated_on DATETIME DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB;
ALTER TABLE api_signers_map_property 
  ADD INDEX `fk.api_signers_map_property.api_signers_id`(api_signers_id);
ALTER TABLE api_signers_map_property 
  ADD INDEX `fk.api_signers_map_property.quorehotel_property_id`(quorehotel_property_id);
ALTER TABLE api_signers_map_property 
  ADD INDEX `idx.api_signers_map_property.signer_hotel_chain`(signer_hotel_chain);
ALTER TABLE api_signers_map_property 
  ADD INDEX `idx.api_signers_map_property.signer_hotel_code`(signer_hotel_code);
ALTER TABLE api_signers_map_property 
  ADD INDEX `idx.api_signers_map_property.signer_hotel_id`(signer_hotel_id);
ALTER TABLE api_signers_map_property 
  ADD INDEX `idx.api_signers_map_property.signer_other_id`(signer_other_id);


CREATE TABLE IF NOT EXISTS api_signers_map_area_status_stay (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  api_signers_id INT(11) NOT NULL,
  signer_area_status_stay_code VARCHAR(50) NOT NULL,
  quorehotel_area_status_stay_id CHAR(3) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB;
ALTER TABLE api_signers_map_area_status_stay 
  ADD INDEX `idx.api_signers_map_area_status_stay.signer_area_status_stay_cod`(signer_area_status_stay_code);


CREATE TABLE IF NOT EXISTS api_signers_map_area_status_service (
  api_signers_id INT(11) NOT NULL,
  signer_area_status_service_code VARCHAR(50) NOT NULL,
  quorehotel_area_status_service_id CHAR(3) DEFAULT NULL,
  PRIMARY KEY (api_signers_id, signer_area_status_service_code)
)
ENGINE = INNODB;
ALTER TABLE api_signers_map_area_status_service 
  ADD INDEX `idx.api_signers_map_area_status_service.signer_area_status_servi`(signer_area_status_service_code);


CREATE TABLE IF NOT EXISTS api_signers_map_area_status_cleaning (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  api_signers_id INT(11) NOT NULL,
  signer_area_status_cleaning_code VARCHAR(50) NOT NULL,
  quorehotel_area_status_cleaning_id CHAR(3) NOT NULL,
  quorehotel_area_status_service_id CHAR(3) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB;
ALTER TABLE api_signers_map_area_status_cleaning 
  ADD INDEX `idx.api_signers_map_area_status_cleaning.signer_area_status_clea`(signer_area_status_cleaning_code);


CREATE TABLE IF NOT EXISTS api_signers_entity (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  api_signers_id INT(11) DEFAULT NULL,
  signer_entity_id VARCHAR(255) DEFAULT NULL,
  type ENUM('Company','TravelAgency','Other') DEFAULT NULL,
  api_signers_map_property_id INT(11) UNSIGNED DEFAULT NULL,
  address_line_1 VARCHAR(255) DEFAULT NULL,
  address_line_2 VARCHAR(255) DEFAULT NULL,
  address_line_3 VARCHAR(255) DEFAULT NULL,
  city VARCHAR(255) DEFAULT NULL,
  state VARCHAR(50) DEFAULT NULL,
  postal_code VARCHAR(50) DEFAULT NULL,
  iso_country_code VARCHAR(10) DEFAULT NULL,
  inserted_on DATETIME DEFAULT CURRENT_TIMESTAMP,
  updated_on DATETIME DEFAULT NULL,
  deactivated_on DATETIME DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB;
ALTER TABLE api_signers_entity 
  ADD INDEX `idx.api_signers_entity_signer_entity_id`(signer_entity_id);
ALTER TABLE api_signers_entity 
  ADD INDEX `idx.api_signers_entity_type`(type);


CREATE TABLE IF NOT EXISTS api_signers_endpoint_operation (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  api_signers_endpoint_id INT(11) UNSIGNED NOT NULL,
  posted_on DATETIME DEFAULT CURRENT_TIMESTAMP,
  event VARCHAR(100) DEFAULT NULL,
  headers TEXT DEFAULT NULL,
  data LONGTEXT DEFAULT NULL,
  post_process_response TEXT DEFAULT NULL,
  post_process_response_date DATETIME DEFAULT NULL,
  post_process_successful TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `data.hotel_id` INT(11) GENERATED ALWAYS AS (json_extract(`data`,'$.hotel_id')) VIRTUAL,
  `data.id` INT(11) GENERATED ALWAYS AS (json_extract(`data`,'$.id')) VIRTUAL,
  `data.object` VARCHAR(255) GENERATED ALWAYS AS (json_unquote(json_extract(`data`,'$.object'))) VIRTUAL,
  PRIMARY KEY (id)
)
ENGINE = INNODB;
ALTER TABLE api_signers_endpoint_operation 
  ADD UNIQUE INDEX `idx.api_signers_endpoint_operation.hotel_id.event.posted_on`(api_signers_endpoint_id, event, posted_on);


CREATE TABLE IF NOT EXISTS api_signers_endpoint (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  api_signers_id INT(11) NOT NULL,
  started_on DATETIME DEFAULT CURRENT_TIMESTAMP,
  ended_on DATETIME DEFAULT NULL,
  endpoint_verb ENUM('GET','POST','PUT') DEFAULT NULL,
  endpoint_url MEDIUMTEXT DEFAULT NULL,
  endpoint_header TEXT DEFAULT NULL,
  endpoint_params TEXT DEFAULT NULL,
  endpoint_body TEXT DEFAULT NULL,
  endpoint_variables JSON DEFAULT NULL,
  action VARCHAR(50) DEFAULT NULL,
  action_regex VARCHAR(255) DEFAULT NULL,
  version CHAR(25) DEFAULT NULL,
  environment CHAR(100) DEFAULT NULL,
  system_name VARCHAR(50) DEFAULT NULL,
  direction ENUM('ToQuore','FromQuore') DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB;


CREATE TABLE IF NOT EXISTS api_signers_authentication (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  api_signers_id INT(11) DEFAULT NULL,
  environment VARCHAR(25) NOT NULL DEFAULT 'development',
  token TEXT DEFAULT NULL,
  last_update DATETIME DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
COMMENT = 'Integrator authentication token information';

# ##################################
# QUOREHOTEL
# ##################################

USE quorehotel;

CREATE TABLE IF NOT EXISTS area_base_event_service_reason (
  area_base_event_service_id INT(11) UNSIGNED NOT NULL,
  area_status_service_reason_id INT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (area_base_event_service_id, area_status_service_reason_id)
)
ENGINE = INNODB,
COMMENT = 'Area Base Event Service Reasons';


CREATE TABLE IF NOT EXISTS area_status_service (
  id CHAR(3) NOT NULL COMMENT 'Area Service Status Id (3 letters)',
  name VARCHAR(50) NOT NULL COMMENT 'Area Service Status Name',
  active TINYINT(1) UNSIGNED DEFAULT 0 COMMENT 'If Area Service Status it is in use',
  PRIMARY KEY (id)
)
ENGINE = INNODB,
COMMENT = 'Area (Stay) Service Status (taxonomy)';


CREATE TABLE IF NOT EXISTS area_base_event_service (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Area Base Service Event Id',
  area_base_id INT(11) UNSIGNED NOT NULL COMMENT 'Area Id',
  area_status_service_id CHAR(3) DEFAULT NULL,
  posted_on DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT 'Date/Time of event (UTC)',
  posted_by INT(11) DEFAULT NULL COMMENT 'User Id of who has posted event (if user)',
  quoreapi_api_signers_id INT(11) DEFAULT NULL COMMENT 'Integrator Id of who has posted event (if integrator)',
  start_datetime DATETIME NOT NULL COMMENT 'Start DateTime of Service Event (Local Time)',
  end_datetime DATETIME NOT NULL COMMENT 'End DateTime of Service Event (Local Time)',
  workorder_id INT(11) DEFAULT NULL COMMENT 'Workorder Id (if service status changed by a workorder)',
  insp_inspection_items INT(11) DEFAULT NULL COMMENT 'Inspection Item Id (if service status changed by a inspection)',
  notes TEXT DEFAULT NULL COMMENT 'Notes about Service',
  PRIMARY KEY (id)
)
ENGINE = INNODB,
COMMENT = 'Area Base Event Service Stream';


CREATE TABLE IF NOT EXISTS stay (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Quore Stay Id',
  quoreapi_api_signers_id INT(11) NOT NULL COMMENT 'API Signer (integrator) Id',
  property_id INT(11) NOT NULL COMMENT 'Property Id',
  reservation_id VARCHAR(255) DEFAULT NULL COMMENT 'PMS Resevation Id',
  booking_confirmation_id VARCHAR(255) DEFAULT NULL COMMENT 'PMS Booking Id',
  stay_group_id VARCHAR(255) DEFAULT NULL COMMENT 'When Stay is part of a Group',
  reference_number VARCHAR(255) DEFAULT NULL COMMENT 'PMS Booking,Stay Reference Id,Number',
  posted_on DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT 'Date of Creation of Stay, Reservation, Booking',
  cancelled_on DATETIME DEFAULT NULL COMMENT 'Date of Cancelation of Stay, Reservation, Booking',
  cancellation_number VARCHAR(255) DEFAULT NULL COMMENT 'PMS Cancellation Number',
  last_updated_on DATETIME DEFAULT NULL COMMENT 'Last Update',
  quoreapi_source_id INT(11) DEFAULT NULL,
  source_info JSON DEFAULT NULL,
  comments TEXT DEFAULT NULL COMMENT 'Any Comments from PMS or Quore',
  PRIMARY KEY (id)
)
ENGINE = INNODB,
COMMENT = 'Bookings or Reservations from Integrator';
ALTER TABLE stay 
  ADD INDEX `idx.stay.booking_confirmation_id`(booking_confirmation_id);
ALTER TABLE stay 
  ADD INDEX `idx.stay.cancellation_number`(cancellation_number);
ALTER TABLE stay 
  ADD INDEX `idx.stay.reference_number`(reference_number);
ALTER TABLE stay 
  ADD INDEX `idx.stay.reservation_id`(reservation_id);
ALTER TABLE stay 
  ADD INDEX `idx.stay.stay_group_id`(stay_group_id);


CREATE TABLE IF NOT EXISTS stay_room (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  stay_id INT(11) UNSIGNED DEFAULT NULL COMMENT 'Stay Id',
  area_base_id INT(11) UNSIGNED DEFAULT NULL COMMENT 'Room Number,Id Mapping (if necessary). Record can exist without room assigned',
  room_rate_code VARCHAR(255) DEFAULT NULL,
  room_block_code VARCHAR(255) DEFAULT NULL,
  posted_on DATETIME DEFAULT CURRENT_TIMESTAMP,
  arrival_date_time DATETIME DEFAULT NULL COMMENT 'Guests Arrival Date and Time (Local Time)',
  departure_date_time DATETIME DEFAULT NULL COMMENT 'Guests Arrival Date and Time (Local Time)',
  adults INT(3) DEFAULT 0,
  children INT(3) DEFAULT 0,
  infant INT(3) DEFAULT 0,
  last_updated_on DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT 'Last Update',
  PRIMARY KEY (id)
)
ENGINE = INNODB,
COMMENT = 'Room(s) assigned to a Stay,Reservation,Booking';


CREATE TABLE IF NOT EXISTS stay_guest (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Stay Guest Id',
  stay_id INT(11) UNSIGNED DEFAULT NULL COMMENT 'Stay,Reservation,Booking Id',
  quoreapi_user_id INT(11) DEFAULT NULL COMMENT 'API User (Guest) Id',
  stay_room_id INT(11) UNSIGNED DEFAULT NULL COMMENT 'Stay Room Id',
  is_main_guest TINYINT(1) UNSIGNED DEFAULT 0 COMMENT 'If this Guest is the Main Guest (Stay Owner) on Stay,Reservation,Booking',
  posted_on DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT 'When Guest was included in Room',
  deleted_on DATETIME DEFAULT NULL COMMENT 'When Guest was removed from Room',
  last_updated_on DATETIME DEFAULT NULL COMMENT 'Last Update',
  PRIMARY KEY (id)
)
ENGINE = INNODB,
COMMENT = 'Guests allocated on Room for a Stay,Reservation,Booking';


CREATE TABLE IF NOT EXISTS area_status_stay (
  id CHAR(3) NOT NULL COMMENT 'Area Status Id (3 letters)',
  name VARCHAR(50) NOT NULL COMMENT 'Area Status Name',
  is_occupied TINYINT(1) UNSIGNED DEFAULT 0 COMMENT 'If this status makes rooms Occupied (if not, it should be Vacant)',
  active TINYINT(1) UNSIGNED DEFAULT 0 COMMENT 'If Area Status has been used',
  PRIMARY KEY (id)
)
ENGINE = INNODB,
COMMENT = 'Area (Stay) Status (taxonomy)';


CREATE TABLE IF NOT EXISTS area_status_service_reason (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Area Base Service Reason Id',
  name VARCHAR(50) NOT NULL COMMENT 'Area Base Service Reason Name',
  api_signers_id INT(11) DEFAULT NULL COMMENT 'Integrator Id (if service originated on Integrator)',
  api_signers_map_property_id INT(11) UNSIGNED DEFAULT NULL COMMENT 'Integrator Map Property Id (if service reason originated on Integrator for a specific property)',
  company_id INT(11) DEFAULT NULL COMMENT 'Company Id (if service reason originated on Company)',
  property_id INT(11) DEFAULT NULL COMMENT 'Property Id (if service reason originated on Property)',
  active TINYINT(1) UNSIGNED DEFAULT 1 COMMENT 'Is Active',
  PRIMARY KEY (id)
)
ENGINE = INNODB,
COMMENT = 'Area Service Status Reasons (taxonomy) (Remark: when api_signers_id AND api_signers_map_property AND company_id AND property_id is null, reason is GLOBAL)';


CREATE TABLE IF NOT EXISTS area_status_cleaning (
  id CHAR(3) NOT NULL COMMENT 'Area Cleaning Status Id (3 letters)',
  name VARCHAR(50) NOT NULL COMMENT 'Area Cleaning Status Name',
  active TINYINT(1) UNSIGNED DEFAULT 0 COMMENT 'If Area Cleaning Status it is in use',
  PRIMARY KEY (id)
)
ENGINE = INNODB,
COMMENT = 'Area (Stay) Status Id (taxonomy)';


CREATE TABLE IF NOT EXISTS area_base_event_stay (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '{ "alias": "AreaEventStayId", "description": "Area Base Event Say Id", "comments": "", "reference": "" }',
  area_base_id INT(11) UNSIGNED DEFAULT NULL COMMENT '{ "alias": "AreaId", "description": "Area Base Id", "reference": "quorehotel.area_base.id" }',
  area_status_stay_id CHAR(3) NOT NULL COMMENT '{ "alias": "AreaStatusStayId", "description": "Area Stay Status Id", "reference": "quorehotel.area_status_stay.id", "comment": ""  }',
  posted_on DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '{ "alias": "PostedDateTime", "description": "Posted On", "comment": "", "reference": "", "time_zone": "UTC" }',
  posted_by INT(11) DEFAULT NULL COMMENT '{ "alias": "PostedByUser", "description": "Posted By", "reference": "quorehotel.user.id", "comment": "if Event was generated by an api_signer, this will be NULL" }',
  stay_room_id INT(11) UNSIGNED DEFAULT NULL COMMENT '{ "alias": "StayRoomId", "description": "Stay Room Id", "comments": "If the room status was changed by a Stay Room record, this field has the Id of the Stay Room", "reference": "quorehotel.stay_room.id" }',
  quoreapi_api_signers_id INT(11) DEFAULT NULL COMMENT '{ "alias": "IntegratorId", "description": Signer Id", "comments": "If the room status was changed by an Integrator, this field has the Id of the Integrator", "reference": "quoreapi.api_signers.id" }',
  PRIMARY KEY (id)
)
ENGINE = INNODB,
COMMENT = 'Area Base Event Stay Stream';


CREATE TABLE IF NOT EXISTS area_base_event_cleaning (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Area Base Event Id',
  area_base_id INT(11) UNSIGNED DEFAULT NULL COMMENT 'Area Id',
  area_status_cleaning_id CHAR(3) NOT NULL COMMENT 'Area Status Cleaning Id',
  posted_on DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Date/Time of event (UTC)',
  posted_by INT(11) DEFAULT NULL COMMENT 'User Id of who has posted event (if user)',
  quoreapi_api_signers_id INT(11) DEFAULT NULL COMMENT 'Integrator Id of who has posted event (if integrator)',
  hk_board_room_id INT(10) UNSIGNED DEFAULT NULL COMMENT 'House Keeping Board Id (if changed by Hk Board)',
  PRIMARY KEY (id)
)
ENGINE = INNODB,
COMMENT = 'Area Base Event Cleaning Stream';