Creator	"DbVis"
Whatever	"Stuff"
graph
[
	label	""
	directed	1
	node
	[
		id	0
		graphics
		[
			x	950.0
			y	200.0
			w	177.0
			h	121.18115234375
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"area_alt_name"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	885.5
			y	169.335693359375
		]
		LabelGraphics
		[
			text	"int(10) unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	946.5
			y	169.335693359375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"property_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	885.5
			y	187.586669921875
		]
		LabelGraphics
		[
			text	"int(10) unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	946.5
			y	187.586669921875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"alt_name"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	885.5
			y	205.837646484375
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	946.5
			y	205.837646484375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"description"
			fontSize	10
			fontName	"Helvetica"
			x	885.5
			y	224.088623046875
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	946.5
			y	224.088623046875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"active"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	885.5
			y	242.339599609375
		]
		LabelGraphics
		[
			text	"tinyint"
			fontSize	10
			fontName	"Helvetica"
			x	946.5
			y	242.339599609375
		]
	]
	node
	[
		id	1
		graphics
		[
			x	950.0
			y	425.0
			w	231.0
			h	175.93408203125
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"area_base"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	858.5
			y	366.959228515625
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	957.5
			y	366.959228515625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"area_type"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	858.5
			y	385.210205078125
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	957.5
			y	385.210205078125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"property_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	858.5
			y	403.461181640625
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	957.5
			y	403.461181640625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"name"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	858.5
			y	421.712158203125
		]
		LabelGraphics
		[
			text	"text"
			fontSize	10
			fontName	"Helvetica"
			x	957.5
			y	421.712158203125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"alt_name_id"
			fontSize	10
			fontName	"Helvetica"
			x	858.5
			y	439.963134765625
		]
		LabelGraphics
		[
			text	"int(10) unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	957.5
			y	439.963134765625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"status_id"
			fontSize	10
			fontName	"Helvetica"
			x	858.5
			y	458.214111328125
		]
		LabelGraphics
		[
			text	"int(4) unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	957.5
			y	458.214111328125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"qac_id"
			fontSize	10
			fontName	"Helvetica"
			x	858.5
			y	476.465087890625
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	957.5
			y	476.465087890625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"property_section_id"
			fontSize	10
			fontName	"Helvetica"
			x	858.5
			y	494.716064453125
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	957.5
			y	494.716064453125
		]
	]
	node
	[
		id	2
		graphics
		[
			x	1375.0
			y	925.0
			w	259.0
			h	157.68310546875
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"area_base_event_cleaning"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1269.5
			y	876.084716796875
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	1396.5
			y	876.084716796875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"area_base_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1269.5
			y	894.335693359375
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	1396.5
			y	894.335693359375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"area_status_cleaning_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1269.5
			y	912.586669921875
		]
		LabelGraphics
		[
			text	"char(3)"
			fontSize	10
			fontName	"Helvetica"
			x	1396.5
			y	912.586669921875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"posted_on"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1269.5
			y	930.837646484375
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	1396.5
			y	930.837646484375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"posted_by"
			fontSize	10
			fontName	"Helvetica"
			x	1269.5
			y	949.088623046875
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1396.5
			y	949.088623046875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"quoreapi_api_signers_id"
			fontSize	10
			fontName	"Helvetica"
			x	1269.5
			y	967.339599609375
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1396.5
			y	967.339599609375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"hk_board_room_id"
			fontSize	10
			fontName	"Helvetica"
			x	1269.5
			y	985.590576171875
		]
		LabelGraphics
		[
			text	"int(10) unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	1396.5
			y	985.590576171875
		]
	]
	node
	[
		id	3
		graphics
		[
			x	1075.0
			y	925.0
			w	162.0
			h	84.67919921875
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"area_status_cleaning"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1018.0
			y	912.586669921875
		]
		LabelGraphics
		[
			text	"char(3)"
			fontSize	10
			fontName	"Helvetica"
			x	1053.0
			y	912.586669921875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"name"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1018.0
			y	930.837646484375
		]
		LabelGraphics
		[
			text	"varchar(50)"
			fontSize	10
			fontName	"Helvetica"
			x	1053.0
			y	930.837646484375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"active"
			fontSize	10
			fontName	"Helvetica"
			x	1018.0
			y	949.088623046875
		]
		LabelGraphics
		[
			text	"tinyint(1) unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	1053.0
			y	949.088623046875
		]
	]
	node
	[
		id	4
		graphics
		[
			x	1375.0
			y	625.0
			w	217.0
			h	285.43994140625
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"hk_board_room"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1290.5
			y	512.206298828125
		]
		LabelGraphics
		[
			text	"int(10) unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	1391.5
			y	512.206298828125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"hk_board_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1290.5
			y	530.457275390625
		]
		LabelGraphics
		[
			text	"int(10) unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	1391.5
			y	530.457275390625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"area_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1290.5
			y	548.708251953125
		]
		LabelGraphics
		[
			text	"int(10) unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	1391.5
			y	548.708251953125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"started"
			fontSize	10
			fontName	"Helvetica"
			x	1290.5
			y	566.959228515625
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	1391.5
			y	566.959228515625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"finished"
			fontSize	10
			fontName	"Helvetica"
			x	1290.5
			y	585.210205078125
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	1391.5
			y	585.210205078125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"total_time"
			fontSize	10
			fontName	"Helvetica"
			x	1290.5
			y	603.461181640625
		]
		LabelGraphics
		[
			text	"time"
			fontSize	10
			fontName	"Helvetica"
			x	1391.5
			y	603.461181640625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"cleaning_id"
			fontSize	10
			fontName	"Helvetica"
			x	1290.5
			y	621.712158203125
		]
		LabelGraphics
		[
			text	"int(10) unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	1391.5
			y	621.712158203125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"special_project_id"
			fontSize	10
			fontName	"Helvetica"
			x	1290.5
			y	639.963134765625
		]
		LabelGraphics
		[
			text	"int(10) unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	1391.5
			y	639.963134765625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"housekeeper_id"
			fontSize	10
			fontName	"Helvetica"
			x	1290.5
			y	658.214111328125
		]
		LabelGraphics
		[
			text	"int(10) unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	1391.5
			y	658.214111328125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"inspector_id"
			fontSize	10
			fontName	"Helvetica"
			x	1290.5
			y	676.465087890625
		]
		LabelGraphics
		[
			text	"int(10) unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	1391.5
			y	676.465087890625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"status"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1290.5
			y	694.716064453125
		]
		LabelGraphics
		[
			text	"enum"
			fontSize	10
			fontName	"Helvetica"
			x	1391.5
			y	694.716064453125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"last_status_update"
			fontSize	10
			fontName	"Helvetica"
			x	1290.5
			y	712.967041015625
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	1391.5
			y	712.967041015625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"priority"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1290.5
			y	731.218017578125
		]
		LabelGraphics
		[
			text	"int(10) unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	1391.5
			y	731.218017578125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"inspect_instance_id"
			fontSize	10
			fontName	"Helvetica"
			x	1290.5
			y	749.468994140625
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1391.5
			y	749.468994140625
		]
	]
	node
	[
		id	5
		graphics
		[
			x	1375.0
			y	1450.0
			w	199.0
			h	741.71435546875
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"user"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1299.5
			y	1109.069091796875
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1109.069091796875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"company_id"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1127.320068359375
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1127.320068359375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"active"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1299.5
			y	1145.571044921875
		]
		LabelGraphics
		[
			text	"tinyint(1)"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1145.571044921875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"admin"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1163.822021484375
		]
		LabelGraphics
		[
			text	"tinyint"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1163.822021484375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"primary_property_id"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1182.072998046875
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1182.072998046875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"department_id"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1200.323974609375
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1200.323974609375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"jobtitle_id"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1218.574951171875
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1218.574951171875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"position"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1236.825927734375
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1236.825927734375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"first_name"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1255.076904296875
		]
		LabelGraphics
		[
			text	"varchar(250)"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1255.076904296875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"last_name"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1273.327880859375
		]
		LabelGraphics
		[
			text	"varchar(250)"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1273.327880859375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"suffix"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1291.578857421875
		]
		LabelGraphics
		[
			text	"varchar(250)"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1291.578857421875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"DOB"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1309.829833984375
		]
		LabelGraphics
		[
			text	"date"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1309.829833984375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"email"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1328.080810546875
		]
		LabelGraphics
		[
			text	"mediumtext"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1328.080810546875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"hashed_password"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1346.331787109375
		]
		LabelGraphics
		[
			text	"varchar(40)"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1346.331787109375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"username"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1364.582763671875
		]
		LabelGraphics
		[
			text	"varchar(100)"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1364.582763671875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"phone"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1382.833740234375
		]
		LabelGraphics
		[
			text	"mediumtext"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1382.833740234375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"ext"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1401.084716796875
		]
		LabelGraphics
		[
			text	"mediumtext"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1401.084716796875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"biography"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1419.335693359375
		]
		LabelGraphics
		[
			text	"mediumtext"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1419.335693359375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"bio_active"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1437.586669921875
		]
		LabelGraphics
		[
			text	"tinyint"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1437.586669921875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"lastpost"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1455.837646484375
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1455.837646484375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"startdate"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1474.088623046875
		]
		LabelGraphics
		[
			text	"date"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1474.088623046875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"profile_img"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1492.339599609375
		]
		LabelGraphics
		[
			text	"mediumtext"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1492.339599609375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"carrier"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1510.590576171875
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1510.590576171875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"onduty"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1528.841552734375
		]
		LabelGraphics
		[
			text	"int(4)"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1528.841552734375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"checkbook"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1547.092529296875
		]
		LabelGraphics
		[
			text	"int(4)"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1547.092529296875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"quoreuser"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1565.343505859375
		]
		LabelGraphics
		[
			text	"int(4)"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1565.343505859375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"gmt"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1583.594482421875
		]
		LabelGraphics
		[
			text	"varchar(45)"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1583.594482421875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"employment_start"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1601.845458984375
		]
		LabelGraphics
		[
			text	"date"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1601.845458984375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"employment_end"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1620.096435546875
		]
		LabelGraphics
		[
			text	"date"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1620.096435546875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"corporate"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1638.347412109375
		]
		LabelGraphics
		[
			text	"tinyint"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1638.347412109375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"autologout"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1656.598388671875
		]
		LabelGraphics
		[
			text	"tinyint(1)"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1656.598388671875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"dailyemails"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1674.849365234375
		]
		LabelGraphics
		[
			text	"tinyint(1)"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1674.849365234375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"can_impersonate"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1693.100341796875
		]
		LabelGraphics
		[
			text	"tinyint(1)"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1693.100341796875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"language_id"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1711.351318359375
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1711.351318359375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"agreement_id"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1729.602294921875
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1729.602294921875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"corporate_job_title"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1747.853271484375
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1747.853271484375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"user_agent"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1766.104248046875
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1766.104248046875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"user_country_id"
			fontSize	10
			fontName	"Helvetica"
			x	1299.5
			y	1784.355224609375
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1784.355224609375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"user_time_notation"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1299.5
			y	1802.606201171875
		]
		LabelGraphics
		[
			text	"tinyint"
			fontSize	10
			fontName	"Helvetica"
			x	1399.5
			y	1802.606201171875
		]
	]
	node
	[
		id	6
		graphics
		[
			x	1700.0
			y	425.0
			w	240.0
			h	230.68701171875
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"area_base_event_service"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1604.0
			y	339.582763671875
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	1730.0
			y	339.582763671875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"area_base_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1604.0
			y	357.833740234375
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	1730.0
			y	357.833740234375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"area_status_service_id"
			fontSize	10
			fontName	"Helvetica"
			x	1604.0
			y	376.084716796875
		]
		LabelGraphics
		[
			text	"char(3)"
			fontSize	10
			fontName	"Helvetica"
			x	1730.0
			y	376.084716796875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"posted_on"
			fontSize	10
			fontName	"Helvetica"
			x	1604.0
			y	394.335693359375
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	1730.0
			y	394.335693359375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"posted_by"
			fontSize	10
			fontName	"Helvetica"
			x	1604.0
			y	412.586669921875
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1730.0
			y	412.586669921875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"quoreapi_api_signers_id"
			fontSize	10
			fontName	"Helvetica"
			x	1604.0
			y	430.837646484375
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1730.0
			y	430.837646484375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"start_datetime"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1604.0
			y	449.088623046875
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	1730.0
			y	449.088623046875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"end_datetime"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1604.0
			y	467.339599609375
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	1730.0
			y	467.339599609375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"workorder_id"
			fontSize	10
			fontName	"Helvetica"
			x	1604.0
			y	485.590576171875
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1730.0
			y	485.590576171875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"insp_inspection_items_id"
			fontSize	10
			fontName	"Helvetica"
			x	1604.0
			y	503.841552734375
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1730.0
			y	503.841552734375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"notes"
			fontSize	10
			fontName	"Helvetica"
			x	1604.0
			y	522.092529296875
		]
		LabelGraphics
		[
			text	"text"
			fontSize	10
			fontName	"Helvetica"
			x	1730.0
			y	522.092529296875
		]
	]
	node
	[
		id	7
		graphics
		[
			x	1700.0
			y	25.0
			w	162.0
			h	84.67919921875
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"area_status_service"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1643.0
			y	12.586669921875
		]
		LabelGraphics
		[
			text	"char(3)"
			fontSize	10
			fontName	"Helvetica"
			x	1678.0
			y	12.586669921875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"name"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1643.0
			y	30.837646484375
		]
		LabelGraphics
		[
			text	"varchar(50)"
			fontSize	10
			fontName	"Helvetica"
			x	1678.0
			y	30.837646484375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"active"
			fontSize	10
			fontName	"Helvetica"
			x	1643.0
			y	49.088623046875
		]
		LabelGraphics
		[
			text	"tinyint(1) unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	1678.0
			y	49.088623046875
		]
	]
	node
	[
		id	8
		graphics
		[
			x	1975.0
			y	425.0
			w	157.0
			h	194.18505859375
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"insp_inspection_items"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1920.5
			y	357.833740234375
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1974.5
			y	357.833740234375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"insp_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1920.5
			y	376.084716796875
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1974.5
			y	376.084716796875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"item_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1920.5
			y	394.335693359375
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1974.5
			y	394.335693359375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"area_id"
			fontSize	10
			fontName	"Helvetica"
			x	1920.5
			y	412.586669921875
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1974.5
			y	412.586669921875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"pass"
			fontSize	10
			fontName	"Helvetica"
			x	1920.5
			y	430.837646484375
		]
		LabelGraphics
		[
			text	"tinyint"
			fontSize	10
			fontName	"Helvetica"
			x	1974.5
			y	430.837646484375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"points"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1920.5
			y	449.088623046875
		]
		LabelGraphics
		[
			text	"tinyint(2)"
			fontSize	10
			fontName	"Helvetica"
			x	1974.5
			y	449.088623046875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"notes"
			fontSize	10
			fontName	"Helvetica"
			x	1920.5
			y	467.339599609375
		]
		LabelGraphics
		[
			text	"text"
			fontSize	10
			fontName	"Helvetica"
			x	1974.5
			y	467.339599609375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"skipped"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1920.5
			y	485.590576171875
		]
		LabelGraphics
		[
			text	"tinyint"
			fontSize	10
			fontName	"Helvetica"
			x	1974.5
			y	485.590576171875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"fail_count"
			fontSize	10
			fontName	"Helvetica"
			x	1920.5
			y	503.841552734375
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1974.5
			y	503.841552734375
		]
	]
	node
	[
		id	9
		graphics
		[
			x	1700.0
			y	1800.0
			w	241.0
			h	632.20849609375
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"workorder"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1603.5
			y	1513.822021484375
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1728.5
			y	1513.822021484375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"active"
			fontSize	10
			fontName	"Helvetica"
			x	1603.5
			y	1532.072998046875
		]
		LabelGraphics
		[
			text	"tinyint"
			fontSize	10
			fontName	"Helvetica"
			x	1728.5
			y	1532.072998046875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"eng_type"
			fontSize	10
			fontName	"Helvetica"
			x	1603.5
			y	1550.323974609375
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1728.5
			y	1550.323974609375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"property_id"
			fontSize	10
			fontName	"Helvetica"
			x	1603.5
			y	1568.574951171875
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1728.5
			y	1568.574951171875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"area"
			fontSize	10
			fontName	"Helvetica"
			x	1603.5
			y	1586.825927734375
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1728.5
			y	1586.825927734375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"eng_item"
			fontSize	10
			fontName	"Helvetica"
			x	1603.5
			y	1605.076904296875
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1728.5
			y	1605.076904296875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"qic_id"
			fontSize	10
			fontName	"Helvetica"
			x	1603.5
			y	1623.327880859375
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1728.5
			y	1623.327880859375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"priority"
			fontSize	10
			fontName	"Helvetica"
			x	1603.5
			y	1641.578857421875
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1728.5
			y	1641.578857421875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"occupied"
			fontSize	10
			fontName	"Helvetica"
			x	1603.5
			y	1659.829833984375
		]
		LabelGraphics
		[
			text	"tinyint"
			fontSize	10
			fontName	"Helvetica"
			x	1728.5
			y	1659.829833984375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"pm_id"
			fontSize	10
			fontName	"Helvetica"
			x	1603.5
			y	1678.080810546875
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1728.5
			y	1678.080810546875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"startdate"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1603.5
			y	1696.331787109375
		]
		LabelGraphics
		[
			text	"timestamp"
			fontSize	10
			fontName	"Helvetica"
			x	1728.5
			y	1696.331787109375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"due"
			fontSize	10
			fontName	"Helvetica"
			x	1603.5
			y	1714.582763671875
		]
		LabelGraphics
		[
			text	"date"
			fontSize	10
			fontName	"Helvetica"
			x	1728.5
			y	1714.582763671875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"discription"
			fontSize	10
			fontName	"Helvetica"
			x	1603.5
			y	1732.833740234375
		]
		LabelGraphics
		[
			text	"text"
			fontSize	10
			fontName	"Helvetica"
			x	1728.5
			y	1732.833740234375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"issue"
			fontSize	10
			fontName	"Helvetica"
			x	1603.5
			y	1751.084716796875
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1728.5
			y	1751.084716796875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"postby"
			fontSize	10
			fontName	"Helvetica"
			x	1603.5
			y	1769.335693359375
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1728.5
			y	1769.335693359375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"assign_to"
			fontSize	10
			fontName	"Helvetica"
			x	1603.5
			y	1787.586669921875
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1728.5
			y	1787.586669921875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"prog_start"
			fontSize	10
			fontName	"Helvetica"
			x	1603.5
			y	1805.837646484375
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	1728.5
			y	1805.837646484375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"completed"
			fontSize	10
			fontName	"Helvetica"
			x	1603.5
			y	1824.088623046875
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	1728.5
			y	1824.088623046875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"completed_by"
			fontSize	10
			fontName	"Helvetica"
			x	1603.5
			y	1842.339599609375
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1728.5
			y	1842.339599609375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"solution"
			fontSize	10
			fontName	"Helvetica"
			x	1603.5
			y	1860.590576171875
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1728.5
			y	1860.590576171875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"notes"
			fontSize	10
			fontName	"Helvetica"
			x	1603.5
			y	1878.841552734375
		]
		LabelGraphics
		[
			text	"text"
			fontSize	10
			fontName	"Helvetica"
			x	1728.5
			y	1878.841552734375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"record_type"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1603.5
			y	1897.092529296875
		]
		LabelGraphics
		[
			text	"varchar(4)"
			fontSize	10
			fontName	"Helvetica"
			x	1728.5
			y	1897.092529296875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"insp_id"
			fontSize	10
			fontName	"Helvetica"
			x	1603.5
			y	1915.343505859375
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1728.5
			y	1915.343505859375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"mg_approved"
			fontSize	10
			fontName	"Helvetica"
			x	1603.5
			y	1933.594482421875
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1728.5
			y	1933.594482421875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"mg_approved_notes"
			fontSize	10
			fontName	"Helvetica"
			x	1603.5
			y	1951.845458984375
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	1728.5
			y	1951.845458984375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"qictd_id"
			fontSize	10
			fontName	"Helvetica"
			x	1603.5
			y	1970.096435546875
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1728.5
			y	1970.096435546875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"department_id"
			fontSize	10
			fontName	"Helvetica"
			x	1603.5
			y	1988.347412109375
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1728.5
			y	1988.347412109375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"api_token_id"
			fontSize	10
			fontName	"Helvetica"
			x	1603.5
			y	2006.598388671875
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1728.5
			y	2006.598388671875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"api_uid"
			fontSize	10
			fontName	"Helvetica"
			x	1603.5
			y	2024.849365234375
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	1728.5
			y	2024.849365234375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"request_when_id"
			fontSize	10
			fontName	"Helvetica"
			x	1603.5
			y	2043.100341796875
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1728.5
			y	2043.100341796875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"cancelled_date"
			fontSize	10
			fontName	"Helvetica"
			x	1603.5
			y	2061.351318359375
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	1728.5
			y	2061.351318359375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"guest_request_origin_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1603.5
			y	2079.602294921875
		]
		LabelGraphics
		[
			text	"int(10) unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	1728.5
			y	2079.602294921875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"parent_complaint_id"
			fontSize	10
			fontName	"Helvetica"
			x	1603.5
			y	2097.853271484375
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1728.5
			y	2097.853271484375
		]
	]
	node
	[
		id	10
		graphics
		[
			x	150.0
			y	100.0
			w	277.0
			h	66.42822265625
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"area_base_event_service_reason"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"area_base_event_service_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	35.5
			y	96.712158203125
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	198.5
			y	96.712158203125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"area_status_service_reason_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	35.5
			y	114.963134765625
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	198.5
			y	114.963134765625
		]
	]
	node
	[
		id	11
		graphics
		[
			x	150.0
			y	300.0
			w	336.0
			h	157.68310546875
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"area_status_service_reason"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	6.0
			y	251.084716796875
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	199.0
			y	251.084716796875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"name"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	6.0
			y	269.335693359375
		]
		LabelGraphics
		[
			text	"varchar(50)"
			fontSize	10
			fontName	"Helvetica"
			x	199.0
			y	269.335693359375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"quoreapi_api_signers_id"
			fontSize	10
			fontName	"Helvetica"
			x	6.0
			y	287.586669921875
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	199.0
			y	287.586669921875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"quoreapi_api_signers_map_property_id"
			fontSize	10
			fontName	"Helvetica"
			x	6.0
			y	305.837646484375
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	199.0
			y	305.837646484375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"company_id"
			fontSize	10
			fontName	"Helvetica"
			x	6.0
			y	324.088623046875
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	199.0
			y	324.088623046875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"property_id"
			fontSize	10
			fontName	"Helvetica"
			x	6.0
			y	342.339599609375
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	199.0
			y	342.339599609375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"active"
			fontSize	10
			fontName	"Helvetica"
			x	6.0
			y	360.590576171875
		]
		LabelGraphics
		[
			text	"tinyint(1) unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	199.0
			y	360.590576171875
		]
	]
	node
	[
		id	12
		graphics
		[
			x	950.0
			y	1450.0
			w	236.0
			h	157.68310546875
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"area_base_event_stay"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	856.0
			y	1401.084716796875
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	978.0
			y	1401.084716796875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"area_base_id"
			fontSize	10
			fontName	"Helvetica"
			x	856.0
			y	1419.335693359375
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	978.0
			y	1419.335693359375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"area_status_stay_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	856.0
			y	1437.586669921875
		]
		LabelGraphics
		[
			text	"char(3)"
			fontSize	10
			fontName	"Helvetica"
			x	978.0
			y	1437.586669921875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"posted_on"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	856.0
			y	1455.837646484375
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	978.0
			y	1455.837646484375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"posted_by"
			fontSize	10
			fontName	"Helvetica"
			x	856.0
			y	1474.088623046875
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	978.0
			y	1474.088623046875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"stay_room_id"
			fontSize	10
			fontName	"Helvetica"
			x	856.0
			y	1492.339599609375
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	978.0
			y	1492.339599609375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"quoreapi_api_signers_id"
			fontSize	10
			fontName	"Helvetica"
			x	856.0
			y	1510.590576171875
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	978.0
			y	1510.590576171875
		]
	]
	node
	[
		id	13
		graphics
		[
			x	950.0
			y	1675.0
			w	193.0
			h	102.93017578125
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"area_status_stay"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	877.5
			y	1653.461181640625
		]
		LabelGraphics
		[
			text	"char(3)"
			fontSize	10
			fontName	"Helvetica"
			x	943.5
			y	1653.461181640625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"name"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	877.5
			y	1671.712158203125
		]
		LabelGraphics
		[
			text	"varchar(50)"
			fontSize	10
			fontName	"Helvetica"
			x	943.5
			y	1671.712158203125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"is_occupied"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	877.5
			y	1689.963134765625
		]
		LabelGraphics
		[
			text	"tinyint(1)"
			fontSize	10
			fontName	"Helvetica"
			x	943.5
			y	1689.963134765625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"active"
			fontSize	10
			fontName	"Helvetica"
			x	877.5
			y	1708.214111328125
		]
		LabelGraphics
		[
			text	"tinyint(1) unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	943.5
			y	1708.214111328125
		]
	]
	node
	[
		id	14
		graphics
		[
			x	650.0
			y	1450.0
			w	220.0
			h	248.93798828125
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"stay_room"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	564.0
			y	1355.457275390625
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	669.0
			y	1355.457275390625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"stay_id"
			fontSize	10
			fontName	"Helvetica"
			x	564.0
			y	1373.708251953125
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	669.0
			y	1373.708251953125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"area_base_id"
			fontSize	10
			fontName	"Helvetica"
			x	564.0
			y	1391.959228515625
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	669.0
			y	1391.959228515625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"room_rate_code"
			fontSize	10
			fontName	"Helvetica"
			x	564.0
			y	1410.210205078125
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	669.0
			y	1410.210205078125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"room_block_code"
			fontSize	10
			fontName	"Helvetica"
			x	564.0
			y	1428.461181640625
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	669.0
			y	1428.461181640625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"posted_on"
			fontSize	10
			fontName	"Helvetica"
			x	564.0
			y	1446.712158203125
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	669.0
			y	1446.712158203125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"arrival_date_time"
			fontSize	10
			fontName	"Helvetica"
			x	564.0
			y	1464.963134765625
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	669.0
			y	1464.963134765625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"departure_date_time"
			fontSize	10
			fontName	"Helvetica"
			x	564.0
			y	1483.214111328125
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	669.0
			y	1483.214111328125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"adults"
			fontSize	10
			fontName	"Helvetica"
			x	564.0
			y	1501.465087890625
		]
		LabelGraphics
		[
			text	"int(3)"
			fontSize	10
			fontName	"Helvetica"
			x	669.0
			y	1501.465087890625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"children"
			fontSize	10
			fontName	"Helvetica"
			x	564.0
			y	1519.716064453125
		]
		LabelGraphics
		[
			text	"int(3)"
			fontSize	10
			fontName	"Helvetica"
			x	669.0
			y	1519.716064453125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"infant"
			fontSize	10
			fontName	"Helvetica"
			x	564.0
			y	1537.967041015625
		]
		LabelGraphics
		[
			text	"int(3)"
			fontSize	10
			fontName	"Helvetica"
			x	669.0
			y	1537.967041015625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"last_updated_on"
			fontSize	10
			fontName	"Helvetica"
			x	564.0
			y	1556.218017578125
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	669.0
			y	1556.218017578125
		]
	]
	node
	[
		id	15
		graphics
		[
			x	500.0
			y	300.0
			w	221.0
			h	321.94189453125
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"company"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	413.5
			y	168.955322265625
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	519.5
			y	168.955322265625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"name"
			fontSize	10
			fontName	"Helvetica"
			x	413.5
			y	187.206298828125
		]
		LabelGraphics
		[
			text	"varchar(250)"
			fontSize	10
			fontName	"Helvetica"
			x	519.5
			y	187.206298828125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"street"
			fontSize	10
			fontName	"Helvetica"
			x	413.5
			y	205.457275390625
		]
		LabelGraphics
		[
			text	"text"
			fontSize	10
			fontName	"Helvetica"
			x	519.5
			y	205.457275390625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"city"
			fontSize	10
			fontName	"Helvetica"
			x	413.5
			y	223.708251953125
		]
		LabelGraphics
		[
			text	"varchar(250)"
			fontSize	10
			fontName	"Helvetica"
			x	519.5
			y	223.708251953125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"state_id"
			fontSize	10
			fontName	"Helvetica"
			x	413.5
			y	241.959228515625
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	519.5
			y	241.959228515625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"zip"
			fontSize	10
			fontName	"Helvetica"
			x	413.5
			y	260.210205078125
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	519.5
			y	260.210205078125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"company_country_id"
			fontSize	10
			fontName	"Helvetica"
			x	413.5
			y	278.461181640625
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	519.5
			y	278.461181640625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"country"
			fontSize	10
			fontName	"Helvetica"
			x	413.5
			y	296.712158203125
		]
		LabelGraphics
		[
			text	"varchar(250)"
			fontSize	10
			fontName	"Helvetica"
			x	519.5
			y	296.712158203125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"phone"
			fontSize	10
			fontName	"Helvetica"
			x	413.5
			y	314.963134765625
		]
		LabelGraphics
		[
			text	"varchar(250)"
			fontSize	10
			fontName	"Helvetica"
			x	519.5
			y	314.963134765625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"email"
			fontSize	10
			fontName	"Helvetica"
			x	413.5
			y	333.214111328125
		]
		LabelGraphics
		[
			text	"text"
			fontSize	10
			fontName	"Helvetica"
			x	519.5
			y	333.214111328125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"website"
			fontSize	10
			fontName	"Helvetica"
			x	413.5
			y	351.465087890625
		]
		LabelGraphics
		[
			text	"text"
			fontSize	10
			fontName	"Helvetica"
			x	519.5
			y	351.465087890625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"active"
			fontSize	10
			fontName	"Helvetica"
			x	413.5
			y	369.716064453125
		]
		LabelGraphics
		[
			text	"tinyint"
			fontSize	10
			fontName	"Helvetica"
			x	519.5
			y	369.716064453125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"startdate"
			fontSize	10
			fontName	"Helvetica"
			x	413.5
			y	387.967041015625
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	519.5
			y	387.967041015625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"lastdate"
			fontSize	10
			fontName	"Helvetica"
			x	413.5
			y	406.218017578125
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	519.5
			y	406.218017578125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"logo_img"
			fontSize	10
			fontName	"Helvetica"
			x	413.5
			y	424.468994140625
		]
		LabelGraphics
		[
			text	"text"
			fontSize	10
			fontName	"Helvetica"
			x	519.5
			y	424.468994140625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"flags"
			fontSize	10
			fontName	"Helvetica"
			x	413.5
			y	442.719970703125
		]
		LabelGraphics
		[
			text	"text"
			fontSize	10
			fontName	"Helvetica"
			x	519.5
			y	442.719970703125
		]
	]
	node
	[
		id	16
		graphics
		[
			x	150.0
			y	875.0
			w	288.0
			h	759.96533203125
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"property"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	30.0
			y	524.943603515625
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	524.943603515625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"company_id"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	543.194580078125
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	543.194580078125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"ownership_id"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	561.445556640625
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	561.445556640625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"name"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	579.696533203125
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	579.696533203125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"type"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	597.947509765625
		]
		LabelGraphics
		[
			text	"tinyint"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	597.947509765625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"active"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	616.198486328125
		]
		LabelGraphics
		[
			text	"tinyint"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	616.198486328125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"startdate"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	634.449462890625
		]
		LabelGraphics
		[
			text	"timestamp"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	634.449462890625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"brand_id"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	652.700439453125
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	652.700439453125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"street"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	670.951416015625
		]
		LabelGraphics
		[
			text	"text"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	670.951416015625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"city"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	689.202392578125
		]
		LabelGraphics
		[
			text	"text"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	689.202392578125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"property_country_id"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	707.453369140625
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	707.453369140625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"state_id"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	725.704345703125
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	725.704345703125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"zip"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	743.955322265625
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	743.955322265625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"latlng"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	762.206298828125
		]
		LabelGraphics
		[
			text	"varchar(30)"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	762.206298828125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"phone"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	780.457275390625
		]
		LabelGraphics
		[
			text	"text"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	780.457275390625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"fax"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	798.708251953125
		]
		LabelGraphics
		[
			text	"text"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	798.708251953125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"tollfree"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	816.959228515625
		]
		LabelGraphics
		[
			text	"text"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	816.959228515625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"email"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	835.210205078125
		]
		LabelGraphics
		[
			text	"text"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	835.210205078125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"website"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	853.461181640625
		]
		LabelGraphics
		[
			text	"text"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	853.461181640625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"description"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	871.712158203125
		]
		LabelGraphics
		[
			text	"text"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	871.712158203125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"position"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	889.963134765625
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	889.963134765625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"profile_img"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	908.214111328125
		]
		LabelGraphics
		[
			text	"text"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	908.214111328125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"initials"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	926.465087890625
		]
		LabelGraphics
		[
			text	"varchar(6)"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	926.465087890625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"roomstatus"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	944.716064453125
		]
		LabelGraphics
		[
			text	"int(4)"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	944.716064453125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"builtdate"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	962.967041015625
		]
		LabelGraphics
		[
			text	"date"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	962.967041015625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"company_region_id"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	981.218017578125
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	981.218017578125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"capex_company_flow_template_id"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	999.468994140625
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	999.468994140625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"live"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	1017.719970703125
		]
		LabelGraphics
		[
			text	"tinyint(1)"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	1017.719970703125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"in_watchlist"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	1035.970947265625
		]
		LabelGraphics
		[
			text	"int(1)"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	1035.970947265625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"is_test"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	1054.221923828125
		]
		LabelGraphics
		[
			text	"int(1)"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	1054.221923828125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"location_pin"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	1072.472900390625
		]
		LabelGraphics
		[
			text	"varchar(6)"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	1072.472900390625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"allowed_hotel_guest_api"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	1090.723876953125
		]
		LabelGraphics
		[
			text	"bit"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	1090.723876953125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"time_zone_name"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	1108.974853515625
		]
		LabelGraphics
		[
			text	"char(64)"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	1108.974853515625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"twilio_subaccount_sid"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	1127.225830078125
		]
		LabelGraphics
		[
			text	"varchar(64)"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	1127.225830078125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"twilio_auth_token"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	1145.476806640625
		]
		LabelGraphics
		[
			text	"char(64)"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	1145.476806640625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"salesforce_id"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	1163.727783203125
		]
		LabelGraphics
		[
			text	"varchar(45)"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	1163.727783203125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"agreement_id"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	1181.978759765625
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	1181.978759765625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"quickbooks_id"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	1200.229736328125
		]
		LabelGraphics
		[
			text	"varchar(45)"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	1200.229736328125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"temperature_scale_override"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	1218.480712890625
		]
		LabelGraphics
		[
			text	"char(1)"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	1218.480712890625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"measurement_system_override"
			fontSize	10
			fontName	"Helvetica"
			x	30.0
			y	1236.731689453125
		]
		LabelGraphics
		[
			text	"char(1)"
			fontSize	10
			fontName	"Helvetica"
			x	203.0
			y	1236.731689453125
		]
	]
	node
	[
		id	17
		graphics
		[
			x	150.0
			y	1450.0
			w	243.0
			h	248.93798828125
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"stay"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	52.5
			y	1355.457275390625
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	180.5
			y	1355.457275390625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"quoreapi_api_signers_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	52.5
			y	1373.708251953125
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	180.5
			y	1373.708251953125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"property_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	52.5
			y	1391.959228515625
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	180.5
			y	1391.959228515625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"reservation_id"
			fontSize	10
			fontName	"Helvetica"
			x	52.5
			y	1410.210205078125
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	180.5
			y	1410.210205078125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"booking_confirmation_id"
			fontSize	10
			fontName	"Helvetica"
			x	52.5
			y	1428.461181640625
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	180.5
			y	1428.461181640625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"stay_group_id"
			fontSize	10
			fontName	"Helvetica"
			x	52.5
			y	1446.712158203125
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	180.5
			y	1446.712158203125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"reference_number"
			fontSize	10
			fontName	"Helvetica"
			x	52.5
			y	1464.963134765625
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	180.5
			y	1464.963134765625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"posted_on"
			fontSize	10
			fontName	"Helvetica"
			x	52.5
			y	1483.214111328125
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	180.5
			y	1483.214111328125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"cancelled_on"
			fontSize	10
			fontName	"Helvetica"
			x	52.5
			y	1501.465087890625
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	180.5
			y	1501.465087890625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"cancellation_number"
			fontSize	10
			fontName	"Helvetica"
			x	52.5
			y	1519.716064453125
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	180.5
			y	1519.716064453125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"last_updated_on"
			fontSize	10
			fontName	"Helvetica"
			x	52.5
			y	1537.967041015625
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	180.5
			y	1537.967041015625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"comments"
			fontSize	10
			fontName	"Helvetica"
			x	52.5
			y	1556.218017578125
		]
		LabelGraphics
		[
			text	"text"
			fontSize	10
			fontName	"Helvetica"
			x	180.5
			y	1556.218017578125
		]
	]
	node
	[
		id	18
		graphics
		[
			x	650.0
			y	2150.0
			w	268.0
			h	504.45166015625
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"user"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	540.0
			y	1927.700439453125
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	709.0
			y	1927.700439453125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"first_name"
			fontSize	10
			fontName	"Helvetica"
			x	540.0
			y	1945.951416015625
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	709.0
			y	1945.951416015625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"last_name"
			fontSize	10
			fontName	"Helvetica"
			x	540.0
			y	1964.202392578125
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	709.0
			y	1964.202392578125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"zip_code"
			fontSize	10
			fontName	"Helvetica"
			x	540.0
			y	1982.453369140625
		]
		LabelGraphics
		[
			text	"varchar(25)"
			fontSize	10
			fontName	"Helvetica"
			x	709.0
			y	1982.453369140625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"created_on"
			fontSize	10
			fontName	"Helvetica"
			x	540.0
			y	2000.704345703125
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	709.0
			y	2000.704345703125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"last_login"
			fontSize	10
			fontName	"Helvetica"
			x	540.0
			y	2018.955322265625
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	709.0
			y	2018.955322265625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"email"
			fontSize	10
			fontName	"Helvetica"
			x	540.0
			y	2037.206298828125
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	709.0
			y	2037.206298828125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"password"
			fontSize	10
			fontName	"Helvetica"
			x	540.0
			y	2055.457275390625
		]
		LabelGraphics
		[
			text	"varchar(45)"
			fontSize	10
			fontName	"Helvetica"
			x	709.0
			y	2055.457275390625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"login_attempts"
			fontSize	10
			fontName	"Helvetica"
			x	540.0
			y	2073.708251953125
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	709.0
			y	2073.708251953125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"blocked_login_attempts"
			fontSize	10
			fontName	"Helvetica"
			x	540.0
			y	2091.959228515625
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	709.0
			y	2091.959228515625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"checkusername_attempts"
			fontSize	10
			fontName	"Helvetica"
			x	540.0
			y	2110.210205078125
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	709.0
			y	2110.210205078125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"blocked_checkusername_attempts"
			fontSize	10
			fontName	"Helvetica"
			x	540.0
			y	2128.461181640625
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	709.0
			y	2128.461181640625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"cell_phone"
			fontSize	10
			fontName	"Helvetica"
			x	540.0
			y	2146.712158203125
		]
		LabelGraphics
		[
			text	"mediumtext"
			fontSize	10
			fontName	"Helvetica"
			x	709.0
			y	2146.712158203125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"cell_stopped"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	540.0
			y	2164.963134765625
		]
		LabelGraphics
		[
			text	"tinyint"
			fontSize	10
			fontName	"Helvetica"
			x	709.0
			y	2164.963134765625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"profile_img"
			fontSize	10
			fontName	"Helvetica"
			x	540.0
			y	2183.214111328125
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	709.0
			y	2183.214111328125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"sms_enabled"
			fontSize	10
			fontName	"Helvetica"
			x	540.0
			y	2201.465087890625
		]
		LabelGraphics
		[
			text	"tinyint"
			fontSize	10
			fontName	"Helvetica"
			x	709.0
			y	2201.465087890625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"prefers_wheelchair"
			fontSize	10
			fontName	"Helvetica"
			x	540.0
			y	2219.716064453125
		]
		LabelGraphics
		[
			text	"tinyint"
			fontSize	10
			fontName	"Helvetica"
			x	709.0
			y	2219.716064453125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"prefers_room_type"
			fontSize	10
			fontName	"Helvetica"
			x	540.0
			y	2237.967041015625
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	709.0
			y	2237.967041015625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"prefers_smoking"
			fontSize	10
			fontName	"Helvetica"
			x	540.0
			y	2256.218017578125
		]
		LabelGraphics
		[
			text	"tinyint"
			fontSize	10
			fontName	"Helvetica"
			x	709.0
			y	2256.218017578125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"prefers_floor_location"
			fontSize	10
			fontName	"Helvetica"
			x	540.0
			y	2274.468994140625
		]
		LabelGraphics
		[
			text	"tinyint"
			fontSize	10
			fontName	"Helvetica"
			x	709.0
			y	2274.468994140625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"prefers_pillow_type"
			fontSize	10
			fontName	"Helvetica"
			x	540.0
			y	2292.719970703125
		]
		LabelGraphics
		[
			text	"tinyint"
			fontSize	10
			fontName	"Helvetica"
			x	709.0
			y	2292.719970703125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"prefers_feather_free"
			fontSize	10
			fontName	"Helvetica"
			x	540.0
			y	2310.970947265625
		]
		LabelGraphics
		[
			text	"tinyint"
			fontSize	10
			fontName	"Helvetica"
			x	709.0
			y	2310.970947265625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"prefers_refrigerator"
			fontSize	10
			fontName	"Helvetica"
			x	540.0
			y	2329.221923828125
		]
		LabelGraphics
		[
			text	"tinyint"
			fontSize	10
			fontName	"Helvetica"
			x	709.0
			y	2329.221923828125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"prefers_extra_towels"
			fontSize	10
			fontName	"Helvetica"
			x	540.0
			y	2347.472900390625
		]
		LabelGraphics
		[
			text	"tinyint"
			fontSize	10
			fontName	"Helvetica"
			x	709.0
			y	2347.472900390625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"prefers_baby_crib"
			fontSize	10
			fontName	"Helvetica"
			x	540.0
			y	2365.723876953125
		]
		LabelGraphics
		[
			text	"tinyint"
			fontSize	10
			fontName	"Helvetica"
			x	709.0
			y	2365.723876953125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"guest_language"
			fontSize	10
			fontName	"Helvetica"
			x	540.0
			y	2383.974853515625
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	709.0
			y	2383.974853515625
		]
	]
	node
	[
		id	19
		graphics
		[
			x	650.0
			y	1725.0
			w	231.0
			h	157.68310546875
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"stay_guest"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	558.5
			y	1676.084716796875
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	646.5
			y	1676.084716796875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"stay_id"
			fontSize	10
			fontName	"Helvetica"
			x	558.5
			y	1694.335693359375
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	646.5
			y	1694.335693359375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"quoreapi_user_id"
			fontSize	10
			fontName	"Helvetica"
			x	558.5
			y	1712.586669921875
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	646.5
			y	1712.586669921875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"stay_room_id"
			fontSize	10
			fontName	"Helvetica"
			x	558.5
			y	1730.837646484375
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	646.5
			y	1730.837646484375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"is_main_guest"
			fontSize	10
			fontName	"Helvetica"
			x	558.5
			y	1749.088623046875
		]
		LabelGraphics
		[
			text	"tinyint(1) unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	646.5
			y	1749.088623046875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"posted_on"
			fontSize	10
			fontName	"Helvetica"
			x	558.5
			y	1767.339599609375
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	646.5
			y	1767.339599609375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"deleted_on"
			fontSize	10
			fontName	"Helvetica"
			x	558.5
			y	1785.590576171875
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	646.5
			y	1785.590576171875
		]
	]
	edge
	[
		source	1
		target	0
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			ySource	-1.0
			yTarget	1.0
		]
	]
	edge
	[
		source	2
		target	1
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	1375.0
					y	925.0
				]
				point
				[
					x	1550.0
					y	925.0
				]
				point
				[
					x	1550.0
					y	450.0
				]
				point
				[
					x	950.0
					y	425.0
				]
			]
		]
		edgeAnchor
		[
			xSource	1.0
			xTarget	1.0
			yTarget	0.2841973602771759
		]
	]
	edge
	[
		source	2
		target	3
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			xSource	-1.0
			xTarget	1.0
		]
	]
	edge
	[
		source	2
		target	4
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			ySource	-1.0
			yTarget	1.0
		]
	]
	edge
	[
		source	2
		target	5
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			ySource	1.0
			yTarget	-1.0
		]
	]
	edge
	[
		source	6
		target	1
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			xSource	-1.0
			xTarget	1.0
		]
	]
	edge
	[
		source	6
		target	7
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			ySource	-1.0
			yTarget	1.0
		]
	]
	edge
	[
		source	6
		target	8
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			xSource	1.0
			xTarget	-1.0
		]
	]
	edge
	[
		source	6
		target	5
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	1700.0
					y	425.0
				]
				point
				[
					x	1675.0
					y	1450.0
				]
				point
				[
					x	1375.0
					y	1450.0
				]
			]
		]
		edgeAnchor
		[
			xSource	-0.2083333283662796
			ySource	1.0
			xTarget	1.0
		]
	]
	edge
	[
		source	6
		target	9
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			ySource	1.0
			yTarget	-1.0
		]
	]
	edge
	[
		source	10
		target	6
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	150.0
					y	100.0
				]
				point
				[
					x	1675.0
					y	100.0
				]
				point
				[
					x	1700.0
					y	425.0
				]
			]
		]
		edgeAnchor
		[
			xSource	1.0
			xTarget	-0.2083333283662796
			yTarget	-1.0
		]
	]
	edge
	[
		source	10
		target	11
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			ySource	1.0
			yTarget	-1.0
		]
	]
	edge
	[
		source	12
		target	1
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			ySource	-1.0
			yTarget	1.0
		]
	]
	edge
	[
		source	12
		target	13
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			ySource	1.0
			yTarget	-1.0
		]
	]
	edge
	[
		source	12
		target	14
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			xSource	-1.0
			xTarget	1.0
		]
	]
	edge
	[
		source	12
		target	5
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			xSource	1.0
			xTarget	-1.0
		]
	]
	edge
	[
		source	11
		target	15
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			xSource	1.0
			xTarget	-1.0
		]
	]
	edge
	[
		source	11
		target	16
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			ySource	1.0
			yTarget	-1.0
		]
	]
	edge
	[
		source	16
		target	15
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	150.0
					y	875.0
				]
				point
				[
					x	500.0
					y	875.0
				]
				point
				[
					x	500.0
					y	300.0
				]
			]
		]
		edgeAnchor
		[
			xSource	1.0
			yTarget	1.0
		]
	]
	edge
	[
		source	14
		target	1
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	650.0
					y	1450.0
				]
				point
				[
					x	650.0
					y	425.0
				]
				point
				[
					x	950.0
					y	425.0
				]
			]
		]
		edgeAnchor
		[
			ySource	-1.0
			xTarget	-1.0
		]
	]
	edge
	[
		source	14
		target	17
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			xSource	-1.0
			xTarget	1.0
		]
	]
	edge
	[
		source	19
		target	18
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			ySource	1.0
			yTarget	-1.0
		]
	]
	edge
	[
		source	19
		target	17
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	650.0
					y	1725.0
				]
				point
				[
					x	150.0
					y	1725.0
				]
				point
				[
					x	150.0
					y	1450.0
				]
			]
		]
		edgeAnchor
		[
			xSource	-1.0
			yTarget	1.0
		]
	]
	edge
	[
		source	19
		target	14
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			ySource	-1.0
			yTarget	1.0
		]
	]
	edge
	[
		source	17
		target	16
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			ySource	-1.0
			yTarget	1.0
		]
	]
]
