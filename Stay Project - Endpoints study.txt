Webhook settings on SNT
https://vendor-synq.quore.com/pms/stayntouch

stay & touch
----------------------------------------------------------------------------------------
Stay
	create_account:
	edit_account
	create_group: N/A
	edit_group: N/A
	create_guest:
	edit_guest:
	anonymize_guest:
	update_inventory:
	create_reservation: call Stay.Insert
	edit_reservation: call Stay.Update
	cancel_reservation: call Stay.Update
	checkin_available: call AreaBaseEventStay.Insert
	checkin_completed: call AreaBaseEventStay.Insert
	checkin_failed: call AreaBaseEventStay.Insert
	checkout_available: call AreaBaseEventStay.Insert
	checkedout: call AreaBaseEventStay.Insert
	pre_checkin_available: call AreaBaseEventStay.Insert
	pre_checkin_success: call AreaBaseEventStay.Insert
	pre_checkin_failure: call AreaBaseEventStay.Insert
	room_assignment_succeed
	room_status
	room_status_service


Stay
----------------------------------------------------------------------------------------
Insert
	add record to stay
	call StayRoom.Insert 
	call StayGuest.Insert (after checking if (quoreapi)User exists - phone number and/or reward number)
	call (quoreapi)User.Insert || (quoreapi)User.Update 

Update
	when updating cancelled_on/cancelation number, call AreaBaseEventStay.Insert (Cancelled)
	call StayRoom.Update (if required)
	call StayGuest.Update (if required)
	
Delete
	N/A
	
Select
	GetStayByStayId
	GetStayGuestsByStayId
	GetStayGuestsByStayRoomId
	GetStayGuestsByStayGroupId (many stays with many rooms)

StayRoom
----------------------------------------------------------------------------------------
Insert
	add record to stay_room
	call AreaBaseEventStay.Insert (Checked In || In House || Waiting List || Pre Check-In || Requested || Reserved || Unknown)
		(observe if room number has been received (status to Pre Check-In *) OR room number not received (status to Reserved || Requested || Waiting List)
	
Update
	update record on stay_room
	if different status, call AreaBaseEventStay.Insert

Delete
	N/A
	
Select (all with guests and all statuses)
	GetStayRoomById
	GetStayRoomsByStayId (can filter by status)
	GetStayRoomByGuestId
	GetStayRoomsByProperty (can filter betweeen arrival and departure)

AreaBaseEventStay
----------------------------------------------------------------------------------------
Insert
	add record to area_base_event_stay

Update
	N/A
	
Delete
	N/A
	
Select
	GetCurrentStayStatusForRoomById
	GetStayStatusHistoryForRoomById
	GetCurrentStayStatusForAllRoomsInProperty
	GetStayStatusHistoryForAllRoomsInProperty

(quoreapi)User (alias Guest)
----------------------------------------------------------------------------------------
Insert
	add record to quoreapi.user

Update
	update record on quoreapi.user
	
Delete
	N/A
	
Select
	GetGuestById
	GetGuestByPhone
	GetGuestByReward
	GetGuestsByIntegrator
	GetGuestStaysHistoryByProperty
	GetGuestStaysHistoryByCompany
	
	
	
	
https://marketplace.atlassian.com/apps/34717/structure-for-jira-projects-at-scale?hosting=cloud&tab=overview
https://marketplace.atlassian.com/apps/1211022/links-hierarchy-for-software-portfolio?hosting=server&tab=overview
https://confluence.atlassian.com/confeval/jira-software-evaluator-resources/jira-software-hierarchy
