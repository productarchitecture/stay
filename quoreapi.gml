Creator	"DbVis"
Whatever	"Stuff"
graph
[
	label	""
	directed	1
	node
	[
		id	0
		graphics
		[
			x	2850.0
			y	225.0
			w	247.0
			h	157.68310546875
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"api_signers"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	2750.5
			y	176.084716796875
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	2898.5
			y	176.084716796875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"signer_name"
			fontSize	10
			fontName	"Helvetica"
			x	2750.5
			y	194.335693359375
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	2898.5
			y	194.335693359375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"active"
			fontSize	10
			fontName	"Helvetica"
			x	2750.5
			y	212.586669921875
		]
		LabelGraphics
		[
			text	"bit"
			fontSize	10
			fontName	"Helvetica"
			x	2898.5
			y	212.586669921875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"remarks"
			fontSize	10
			fontName	"Helvetica"
			x	2750.5
			y	230.837646484375
		]
		LabelGraphics
		[
			text	"text"
			fontSize	10
			fontName	"Helvetica"
			x	2898.5
			y	230.837646484375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"allow_use_of_all_areas"
			fontSize	10
			fontName	"Helvetica"
			x	2750.5
			y	249.088623046875
		]
		LabelGraphics
		[
			text	"bit"
			fontSize	10
			fontName	"Helvetica"
			x	2898.5
			y	249.088623046875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"guest_name_to_be_displayed"
			fontSize	10
			fontName	"Helvetica"
			x	2750.5
			y	267.339599609375
		]
		LabelGraphics
		[
			text	"varchar(45)"
			fontSize	10
			fontName	"Helvetica"
			x	2898.5
			y	267.339599609375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"automatic_callbacks"
			fontSize	10
			fontName	"Helvetica"
			x	2750.5
			y	285.590576171875
		]
		LabelGraphics
		[
			text	"tinyint(1)"
			fontSize	10
			fontName	"Helvetica"
			x	2898.5
			y	285.590576171875
		]
	]
	node
	[
		id	1
		graphics
		[
			x	800.0
			y	425.0
			w	259.0
			h	139.43212890625
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"api_signers_authentication"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	694.5
			y	385.210205078125
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	839.5
			y	385.210205078125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"api_signers_id"
			fontSize	10
			fontName	"Helvetica"
			x	694.5
			y	403.461181640625
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	839.5
			y	403.461181640625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"api_signers_map_property_id"
			fontSize	10
			fontName	"Helvetica"
			x	694.5
			y	421.712158203125
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	839.5
			y	421.712158203125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"environment"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	694.5
			y	439.963134765625
		]
		LabelGraphics
		[
			text	"varchar(25)"
			fontSize	10
			fontName	"Helvetica"
			x	839.5
			y	439.963134765625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"token"
			fontSize	10
			fontName	"Helvetica"
			x	694.5
			y	458.214111328125
		]
		LabelGraphics
		[
			text	"text"
			fontSize	10
			fontName	"Helvetica"
			x	839.5
			y	458.214111328125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"last_update"
			fontSize	10
			fontName	"Helvetica"
			x	694.5
			y	476.465087890625
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	839.5
			y	476.465087890625
		]
	]
	node
	[
		id	2
		graphics
		[
			x	475.0
			y	1025.0
			w	231.0
			h	230.68701171875
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"api_signers_map_property"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	383.5
			y	939.582763671875
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	499.5
			y	939.582763671875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"api_signers_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	383.5
			y	957.833740234375
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	499.5
			y	957.833740234375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"quorehotel_property_id"
			fontSize	10
			fontName	"Helvetica"
			x	383.5
			y	976.084716796875
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	499.5
			y	976.084716796875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"signer_hotel_id"
			fontSize	10
			fontName	"Helvetica"
			x	383.5
			y	994.335693359375
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	499.5
			y	994.335693359375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"signer_other_id"
			fontSize	10
			fontName	"Helvetica"
			x	383.5
			y	1012.586669921875
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	499.5
			y	1012.586669921875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"signer_hotel_code"
			fontSize	10
			fontName	"Helvetica"
			x	383.5
			y	1030.837646484375
		]
		LabelGraphics
		[
			text	"varchar(25)"
			fontSize	10
			fontName	"Helvetica"
			x	499.5
			y	1030.837646484375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"signer_hotel_name"
			fontSize	10
			fontName	"Helvetica"
			x	383.5
			y	1049.088623046875
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	499.5
			y	1049.088623046875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"signer_hotel_chain"
			fontSize	10
			fontName	"Helvetica"
			x	383.5
			y	1067.339599609375
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	499.5
			y	1067.339599609375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"signer_hotel_brand"
			fontSize	10
			fontName	"Helvetica"
			x	383.5
			y	1085.590576171875
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	499.5
			y	1085.590576171875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"inserted_on"
			fontSize	10
			fontName	"Helvetica"
			x	383.5
			y	1103.841552734375
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	499.5
			y	1103.841552734375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"deactivated_on"
			fontSize	10
			fontName	"Helvetica"
			x	383.5
			y	1122.092529296875
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	499.5
			y	1122.092529296875
		]
	]
	node
	[
		id	3
		graphics
		[
			x	1125.0
			y	525.0
			w	260.0
			h	303.69091796875
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"api_signers_endpoint"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1019.0
			y	403.080810546875
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	1164.0
			y	403.080810546875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"api_signers_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1019.0
			y	421.331787109375
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1164.0
			y	421.331787109375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"api_signers_map_property_id"
			fontSize	10
			fontName	"Helvetica"
			x	1019.0
			y	439.582763671875
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	1164.0
			y	439.582763671875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"started_on"
			fontSize	10
			fontName	"Helvetica"
			x	1019.0
			y	457.833740234375
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	1164.0
			y	457.833740234375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"ended_on"
			fontSize	10
			fontName	"Helvetica"
			x	1019.0
			y	476.084716796875
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	1164.0
			y	476.084716796875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"endpoint_verb"
			fontSize	10
			fontName	"Helvetica"
			x	1019.0
			y	494.335693359375
		]
		LabelGraphics
		[
			text	"enum"
			fontSize	10
			fontName	"Helvetica"
			x	1164.0
			y	494.335693359375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"endpoint_url"
			fontSize	10
			fontName	"Helvetica"
			x	1019.0
			y	512.586669921875
		]
		LabelGraphics
		[
			text	"mediumtext"
			fontSize	10
			fontName	"Helvetica"
			x	1164.0
			y	512.586669921875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"endpoint_header"
			fontSize	10
			fontName	"Helvetica"
			x	1019.0
			y	530.837646484375
		]
		LabelGraphics
		[
			text	"text"
			fontSize	10
			fontName	"Helvetica"
			x	1164.0
			y	530.837646484375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"endpoint_body"
			fontSize	10
			fontName	"Helvetica"
			x	1019.0
			y	549.088623046875
		]
		LabelGraphics
		[
			text	"text"
			fontSize	10
			fontName	"Helvetica"
			x	1164.0
			y	549.088623046875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"action"
			fontSize	10
			fontName	"Helvetica"
			x	1019.0
			y	567.339599609375
		]
		LabelGraphics
		[
			text	"varchar(50)"
			fontSize	10
			fontName	"Helvetica"
			x	1164.0
			y	567.339599609375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"version"
			fontSize	10
			fontName	"Helvetica"
			x	1019.0
			y	585.590576171875
		]
		LabelGraphics
		[
			text	"char(25)"
			fontSize	10
			fontName	"Helvetica"
			x	1164.0
			y	585.590576171875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"environment"
			fontSize	10
			fontName	"Helvetica"
			x	1019.0
			y	603.841552734375
		]
		LabelGraphics
		[
			text	"char(100)"
			fontSize	10
			fontName	"Helvetica"
			x	1164.0
			y	603.841552734375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"system_name"
			fontSize	10
			fontName	"Helvetica"
			x	1019.0
			y	622.092529296875
		]
		LabelGraphics
		[
			text	"varchar(50)"
			fontSize	10
			fontName	"Helvetica"
			x	1164.0
			y	622.092529296875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"direction"
			fontSize	10
			fontName	"Helvetica"
			x	1019.0
			y	640.343505859375
		]
		LabelGraphics
		[
			text	"enum"
			fontSize	10
			fontName	"Helvetica"
			x	1164.0
			y	640.343505859375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"signature"
			fontSize	10
			fontName	"Helvetica"
			x	1019.0
			y	658.594482421875
		]
		LabelGraphics
		[
			text	"varchar(100)"
			fontSize	10
			fontName	"Helvetica"
			x	1164.0
			y	658.594482421875
		]
	]
	node
	[
		id	4
		graphics
		[
			x	1475.0
			y	525.0
			w	283.0
			h	194.18505859375
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"api_signers_endpoint_operation"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1357.5
			y	457.833740234375
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	1497.5
			y	457.833740234375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"api_signers_endpoint_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1357.5
			y	476.084716796875
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	1497.5
			y	476.084716796875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"posted_on"
			fontSize	10
			fontName	"Helvetica"
			x	1357.5
			y	494.335693359375
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	1497.5
			y	494.335693359375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"event"
			fontSize	10
			fontName	"Helvetica"
			x	1357.5
			y	512.586669921875
		]
		LabelGraphics
		[
			text	"varchar(100)"
			fontSize	10
			fontName	"Helvetica"
			x	1497.5
			y	512.586669921875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"headers"
			fontSize	10
			fontName	"Helvetica"
			x	1357.5
			y	530.837646484375
		]
		LabelGraphics
		[
			text	"text"
			fontSize	10
			fontName	"Helvetica"
			x	1497.5
			y	530.837646484375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"data"
			fontSize	10
			fontName	"Helvetica"
			x	1357.5
			y	549.088623046875
		]
		LabelGraphics
		[
			text	"longtext"
			fontSize	10
			fontName	"Helvetica"
			x	1497.5
			y	549.088623046875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"post_process_response"
			fontSize	10
			fontName	"Helvetica"
			x	1357.5
			y	567.339599609375
		]
		LabelGraphics
		[
			text	"text"
			fontSize	10
			fontName	"Helvetica"
			x	1497.5
			y	567.339599609375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"post_process_response_date"
			fontSize	10
			fontName	"Helvetica"
			x	1357.5
			y	585.590576171875
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	1497.5
			y	585.590576171875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"post_process_successful"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1357.5
			y	603.841552734375
		]
		LabelGraphics
		[
			text	"tinyint(1) unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	1497.5
			y	603.841552734375
		]
	]
	node
	[
		id	5
		graphics
		[
			x	1800.0
			y	725.0
			w	260.0
			h	303.69091796875
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"api_signers_entity"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1694.0
			y	603.080810546875
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	1839.0
			y	603.080810546875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"api_signers_id"
			fontSize	10
			fontName	"Helvetica"
			x	1694.0
			y	621.331787109375
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	1839.0
			y	621.331787109375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"signer_entity_id"
			fontSize	10
			fontName	"Helvetica"
			x	1694.0
			y	639.582763671875
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	1839.0
			y	639.582763671875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"type"
			fontSize	10
			fontName	"Helvetica"
			x	1694.0
			y	657.833740234375
		]
		LabelGraphics
		[
			text	"enum"
			fontSize	10
			fontName	"Helvetica"
			x	1839.0
			y	657.833740234375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"api_signers_map_property_id"
			fontSize	10
			fontName	"Helvetica"
			x	1694.0
			y	676.084716796875
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	1839.0
			y	676.084716796875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"address_line_1"
			fontSize	10
			fontName	"Helvetica"
			x	1694.0
			y	694.335693359375
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	1839.0
			y	694.335693359375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"address_line_2"
			fontSize	10
			fontName	"Helvetica"
			x	1694.0
			y	712.586669921875
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	1839.0
			y	712.586669921875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"address_line_3"
			fontSize	10
			fontName	"Helvetica"
			x	1694.0
			y	730.837646484375
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	1839.0
			y	730.837646484375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"city"
			fontSize	10
			fontName	"Helvetica"
			x	1694.0
			y	749.088623046875
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	1839.0
			y	749.088623046875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"state"
			fontSize	10
			fontName	"Helvetica"
			x	1694.0
			y	767.339599609375
		]
		LabelGraphics
		[
			text	"varchar(50)"
			fontSize	10
			fontName	"Helvetica"
			x	1839.0
			y	767.339599609375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"postal_code"
			fontSize	10
			fontName	"Helvetica"
			x	1694.0
			y	785.590576171875
		]
		LabelGraphics
		[
			text	"varchar(50)"
			fontSize	10
			fontName	"Helvetica"
			x	1839.0
			y	785.590576171875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"iso_country_code"
			fontSize	10
			fontName	"Helvetica"
			x	1694.0
			y	803.841552734375
		]
		LabelGraphics
		[
			text	"varchar(10)"
			fontSize	10
			fontName	"Helvetica"
			x	1839.0
			y	803.841552734375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"inserted_on"
			fontSize	10
			fontName	"Helvetica"
			x	1694.0
			y	822.092529296875
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	1839.0
			y	822.092529296875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"updated_on"
			fontSize	10
			fontName	"Helvetica"
			x	1694.0
			y	840.343505859375
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	1839.0
			y	840.343505859375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"deactivated_on"
			fontSize	10
			fontName	"Helvetica"
			x	1694.0
			y	858.594482421875
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	1839.0
			y	858.594482421875
		]
	]
	node
	[
		id	6
		graphics
		[
			x	300.0
			y	225.0
			w	299.0
			h	121.18115234375
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"api_signers_map_area_status_cleaning"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	174.5
			y	194.335693359375
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	359.5
			y	194.335693359375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"api_signers_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	174.5
			y	212.586669921875
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	359.5
			y	212.586669921875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"signer_area_status_cleaning_code"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	174.5
			y	230.837646484375
		]
		LabelGraphics
		[
			text	"varchar(50)"
			fontSize	10
			fontName	"Helvetica"
			x	359.5
			y	230.837646484375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"quorehotel_area_status_cleaning_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	174.5
			y	249.088623046875
		]
		LabelGraphics
		[
			text	"char(3)"
			fontSize	10
			fontName	"Helvetica"
			x	359.5
			y	249.088623046875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"quorehotel_area_status_service_id"
			fontSize	10
			fontName	"Helvetica"
			x	174.5
			y	267.339599609375
		]
		LabelGraphics
		[
			text	"char(3)"
			fontSize	10
			fontName	"Helvetica"
			x	359.5
			y	267.339599609375
		]
	]
	node
	[
		id	7
		graphics
		[
			x	2850.0
			y	25.0
			w	284.0
			h	84.67919921875
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"api_signers_map_area_status_service"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"api_signers_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	2732.0
			y	12.586669921875
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	2907.0
			y	12.586669921875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"signer_area_status_service_code"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	2732.0
			y	30.837646484375
		]
		LabelGraphics
		[
			text	"varchar(50)"
			fontSize	10
			fontName	"Helvetica"
			x	2907.0
			y	30.837646484375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"quorehotel_area_status_service_id"
			fontSize	10
			fontName	"Helvetica"
			x	2732.0
			y	49.088623046875
		]
		LabelGraphics
		[
			text	"char(3)"
			fontSize	10
			fontName	"Helvetica"
			x	2907.0
			y	49.088623046875
		]
	]
	node
	[
		id	8
		graphics
		[
			x	1975.0
			y	1025.0
			w	336.0
			h	139.43212890625
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"api_signers_map_area_status_service_reason"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1831.0
			y	985.210205078125
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	2052.0
			y	985.210205078125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"api_signers_id"
			fontSize	10
			fontName	"Helvetica"
			x	1831.0
			y	1003.461181640625
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	2052.0
			y	1003.461181640625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"api_signers_map_property_id"
			fontSize	10
			fontName	"Helvetica"
			x	1831.0
			y	1021.712158203125
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	2052.0
			y	1021.712158203125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"signer_reason_code"
			fontSize	10
			fontName	"Helvetica"
			x	1831.0
			y	1039.963134765625
		]
		LabelGraphics
		[
			text	"varchar(50)"
			fontSize	10
			fontName	"Helvetica"
			x	2052.0
			y	1039.963134765625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"signer_reason"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1831.0
			y	1058.214111328125
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	2052.0
			y	1058.214111328125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"quorehotel_area_status_service_reason_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1831.0
			y	1076.465087890625
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	2052.0
			y	1076.465087890625
		]
	]
	node
	[
		id	9
		graphics
		[
			x	3175.0
			y	225.0
			w	277.0
			h	102.93017578125
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"api_signers_map_area_status_stay"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	3060.5
			y	203.461181640625
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	3223.5
			y	203.461181640625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"api_signers_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	3060.5
			y	221.712158203125
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	3223.5
			y	221.712158203125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"signer_area_status_stay_code"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	3060.5
			y	239.963134765625
		]
		LabelGraphics
		[
			text	"varchar(50)"
			fontSize	10
			fontName	"Helvetica"
			x	3223.5
			y	239.963134765625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"quorehotel_area_status_stay_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	3060.5
			y	258.214111328125
		]
		LabelGraphics
		[
			text	"char(3)"
			fontSize	10
			fontName	"Helvetica"
			x	3223.5
			y	258.214111328125
		]
	]
	node
	[
		id	10
		graphics
		[
			x	475.0
			y	1275.0
			w	312.0
			h	139.43212890625
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"api_signers_map_property_area"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	343.0
			y	1235.210205078125
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	541.0
			y	1235.210205078125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"api_signers_map_property_id"
			fontSize	10
			fontName	"Helvetica"
			x	343.0
			y	1253.461181640625
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	541.0
			y	1253.461181640625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"signer_room_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	343.0
			y	1271.712158203125
		]
		LabelGraphics
		[
			text	"varchar(50)"
			fontSize	10
			fontName	"Helvetica"
			x	541.0
			y	1271.712158203125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"signer_room_name"
			fontSize	10
			fontName	"Helvetica"
			x	343.0
			y	1289.963134765625
		]
		LabelGraphics
		[
			text	"varchar(50)"
			fontSize	10
			fontName	"Helvetica"
			x	541.0
			y	1289.963134765625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"quorehotel_area_base_id"
			fontSize	10
			fontName	"Helvetica"
			x	343.0
			y	1308.214111328125
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	541.0
			y	1308.214111328125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"api_signers_map_property_area_type_id"
			fontSize	10
			fontName	"Helvetica"
			x	343.0
			y	1326.465087890625
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	541.0
			y	1326.465087890625
		]
	]
	node
	[
		id	11
		graphics
		[
			x	850.0
			y	1275.0
			w	282.0
			h	139.43212890625
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"api_signers_map_property_area_type"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	733.0
			y	1235.210205078125
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	883.0
			y	1235.210205078125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"api_signers_map_property_id"
			fontSize	10
			fontName	"Helvetica"
			x	733.0
			y	1253.461181640625
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	883.0
			y	1253.461181640625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"signer_room_type_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	733.0
			y	1271.712158203125
		]
		LabelGraphics
		[
			text	"varchar(50)"
			fontSize	10
			fontName	"Helvetica"
			x	883.0
			y	1271.712158203125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"signer_room_type_code"
			fontSize	10
			fontName	"Helvetica"
			x	733.0
			y	1289.963134765625
		]
		LabelGraphics
		[
			text	"varchar(50)"
			fontSize	10
			fontName	"Helvetica"
			x	883.0
			y	1289.963134765625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"signer_room_type_name"
			fontSize	10
			fontName	"Helvetica"
			x	733.0
			y	1308.214111328125
		]
		LabelGraphics
		[
			text	"varchar(50)"
			fontSize	10
			fontName	"Helvetica"
			x	883.0
			y	1308.214111328125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"quorehotel_area_alt_name_id"
			fontSize	10
			fontName	"Helvetica"
			x	733.0
			y	1326.465087890625
		]
		LabelGraphics
		[
			text	"int(10) unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	883.0
			y	1326.465087890625
		]
	]
	node
	[
		id	12
		graphics
		[
			x	125.0
			y	1450.0
			w	295.0
			h	175.93408203125
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"api_signers_map_property_rate"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1.5
			y	1391.959228515625
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	153.5
			y	1391.959228515625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"api_signers_map_property_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1.5
			y	1410.210205078125
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	153.5
			y	1410.210205078125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"signer_rate_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	1.5
			y	1428.461181640625
		]
		LabelGraphics
		[
			text	"varchar(25)"
			fontSize	10
			fontName	"Helvetica"
			x	153.5
			y	1428.461181640625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"signer_rate_code"
			fontSize	10
			fontName	"Helvetica"
			x	1.5
			y	1446.712158203125
		]
		LabelGraphics
		[
			text	"varchar(25)"
			fontSize	10
			fontName	"Helvetica"
			x	153.5
			y	1446.712158203125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"start_date"
			fontSize	10
			fontName	"Helvetica"
			x	1.5
			y	1464.963134765625
		]
		LabelGraphics
		[
			text	"date"
			fontSize	10
			fontName	"Helvetica"
			x	153.5
			y	1464.963134765625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"end_date"
			fontSize	10
			fontName	"Helvetica"
			x	1.5
			y	1483.214111328125
		]
		LabelGraphics
		[
			text	"date"
			fontSize	10
			fontName	"Helvetica"
			x	153.5
			y	1483.214111328125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"is_active"
			fontSize	10
			fontName	"Helvetica"
			x	1.5
			y	1501.465087890625
		]
		LabelGraphics
		[
			text	"tinyint(1) unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	153.5
			y	1501.465087890625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"quorehotel_currency_code_id"
			fontSize	10
			fontName	"Helvetica"
			x	1.5
			y	1519.716064453125
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	153.5
			y	1519.716064453125
		]
	]
	node
	[
		id	13
		graphics
		[
			x	850.0
			y	1450.0
			w	320.0
			h	66.42822265625
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"api_signers_map_property_rate_area_type"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"api_signers_map_property_rate_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	714.0
			y	1446.712158203125
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	920.0
			y	1446.712158203125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"api_signers_map_property_area_type_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	714.0
			y	1464.963134765625
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	920.0
			y	1464.963134765625
		]
	]
	node
	[
		id	14
		graphics
		[
			x	2850.0
			y	450.0
			w	269.0
			h	121.18115234375
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"api_signers_map_reward"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	2739.5
			y	419.335693359375
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	2893.5
			y	419.335693359375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"api_signers_id"
			fontSize	10
			fontName	"Helvetica"
			x	2739.5
			y	437.586669921875
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	2893.5
			y	437.586669921875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"signer_reward_id"
			fontSize	10
			fontName	"Helvetica"
			x	2739.5
			y	455.837646484375
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	2893.5
			y	455.837646484375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"signer_reward_name"
			fontSize	10
			fontName	"Helvetica"
			x	2739.5
			y	474.088623046875
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	2893.5
			y	474.088623046875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"quorehotel_reward_program_id"
			fontSize	10
			fontName	"Helvetica"
			x	2739.5
			y	492.339599609375
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	2893.5
			y	492.339599609375
		]
	]
	node
	[
		id	15
		graphics
		[
			x	2500.0
			y	650.0
			w	255.0
			h	194.18505859375
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"api_signers_map_user"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	2396.5
			y	582.833740234375
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	2536.5
			y	582.833740234375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"api_signers_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	2396.5
			y	601.084716796875
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	2536.5
			y	601.084716796875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"signer_guest_id"
			fontSize	10
			fontName	"Helvetica"
			x	2396.5
			y	619.335693359375
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	2536.5
			y	619.335693359375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"signer_guest_first_name"
			fontSize	10
			fontName	"Helvetica"
			x	2396.5
			y	637.586669921875
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	2536.5
			y	637.586669921875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"signer_guest_last_name"
			fontSize	10
			fontName	"Helvetica"
			x	2396.5
			y	655.837646484375
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	2536.5
			y	655.837646484375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"signer_guest_mobile_phone"
			fontSize	10
			fontName	"Helvetica"
			x	2396.5
			y	674.088623046875
		]
		LabelGraphics
		[
			text	"varchar(100)"
			fontSize	10
			fontName	"Helvetica"
			x	2536.5
			y	674.088623046875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"signer_guest_email"
			fontSize	10
			fontName	"Helvetica"
			x	2396.5
			y	692.339599609375
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	2536.5
			y	692.339599609375
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"last_update"
			fontSize	10
			fontName	"Helvetica"
			x	2396.5
			y	710.590576171875
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	2536.5
			y	710.590576171875
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"user_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	2396.5
			y	728.841552734375
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	2536.5
			y	728.841552734375
		]
	]
	node
	[
		id	16
		graphics
		[
			x	2150.0
			y	650.0
			w	268.0
			h	504.45166015625
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"user"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	2040.0
			y	427.700439453125
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	2209.0
			y	427.700439453125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"first_name"
			fontSize	10
			fontName	"Helvetica"
			x	2040.0
			y	445.951416015625
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	2209.0
			y	445.951416015625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"last_name"
			fontSize	10
			fontName	"Helvetica"
			x	2040.0
			y	464.202392578125
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	2209.0
			y	464.202392578125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"zip_code"
			fontSize	10
			fontName	"Helvetica"
			x	2040.0
			y	482.453369140625
		]
		LabelGraphics
		[
			text	"varchar(25)"
			fontSize	10
			fontName	"Helvetica"
			x	2209.0
			y	482.453369140625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"created_on"
			fontSize	10
			fontName	"Helvetica"
			x	2040.0
			y	500.704345703125
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	2209.0
			y	500.704345703125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"last_login"
			fontSize	10
			fontName	"Helvetica"
			x	2040.0
			y	518.955322265625
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	2209.0
			y	518.955322265625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"email"
			fontSize	10
			fontName	"Helvetica"
			x	2040.0
			y	537.206298828125
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	2209.0
			y	537.206298828125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"password"
			fontSize	10
			fontName	"Helvetica"
			x	2040.0
			y	555.457275390625
		]
		LabelGraphics
		[
			text	"varchar(45)"
			fontSize	10
			fontName	"Helvetica"
			x	2209.0
			y	555.457275390625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"login_attempts"
			fontSize	10
			fontName	"Helvetica"
			x	2040.0
			y	573.708251953125
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	2209.0
			y	573.708251953125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"blocked_login_attempts"
			fontSize	10
			fontName	"Helvetica"
			x	2040.0
			y	591.959228515625
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	2209.0
			y	591.959228515625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"checkusername_attempts"
			fontSize	10
			fontName	"Helvetica"
			x	2040.0
			y	610.210205078125
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	2209.0
			y	610.210205078125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"blocked_checkusername_attempts"
			fontSize	10
			fontName	"Helvetica"
			x	2040.0
			y	628.461181640625
		]
		LabelGraphics
		[
			text	"datetime"
			fontSize	10
			fontName	"Helvetica"
			x	2209.0
			y	628.461181640625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"cell_phone"
			fontSize	10
			fontName	"Helvetica"
			x	2040.0
			y	646.712158203125
		]
		LabelGraphics
		[
			text	"mediumtext"
			fontSize	10
			fontName	"Helvetica"
			x	2209.0
			y	646.712158203125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"cell_stopped"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	2040.0
			y	664.963134765625
		]
		LabelGraphics
		[
			text	"tinyint"
			fontSize	10
			fontName	"Helvetica"
			x	2209.0
			y	664.963134765625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"profile_img"
			fontSize	10
			fontName	"Helvetica"
			x	2040.0
			y	683.214111328125
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	2209.0
			y	683.214111328125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"sms_enabled"
			fontSize	10
			fontName	"Helvetica"
			x	2040.0
			y	701.465087890625
		]
		LabelGraphics
		[
			text	"tinyint"
			fontSize	10
			fontName	"Helvetica"
			x	2209.0
			y	701.465087890625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"prefers_wheelchair"
			fontSize	10
			fontName	"Helvetica"
			x	2040.0
			y	719.716064453125
		]
		LabelGraphics
		[
			text	"tinyint"
			fontSize	10
			fontName	"Helvetica"
			x	2209.0
			y	719.716064453125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"prefers_room_type"
			fontSize	10
			fontName	"Helvetica"
			x	2040.0
			y	737.967041015625
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	2209.0
			y	737.967041015625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"prefers_smoking"
			fontSize	10
			fontName	"Helvetica"
			x	2040.0
			y	756.218017578125
		]
		LabelGraphics
		[
			text	"tinyint"
			fontSize	10
			fontName	"Helvetica"
			x	2209.0
			y	756.218017578125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"prefers_floor_location"
			fontSize	10
			fontName	"Helvetica"
			x	2040.0
			y	774.468994140625
		]
		LabelGraphics
		[
			text	"tinyint"
			fontSize	10
			fontName	"Helvetica"
			x	2209.0
			y	774.468994140625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"prefers_pillow_type"
			fontSize	10
			fontName	"Helvetica"
			x	2040.0
			y	792.719970703125
		]
		LabelGraphics
		[
			text	"tinyint"
			fontSize	10
			fontName	"Helvetica"
			x	2209.0
			y	792.719970703125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"prefers_feather_free"
			fontSize	10
			fontName	"Helvetica"
			x	2040.0
			y	810.970947265625
		]
		LabelGraphics
		[
			text	"tinyint"
			fontSize	10
			fontName	"Helvetica"
			x	2209.0
			y	810.970947265625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"prefers_refrigerator"
			fontSize	10
			fontName	"Helvetica"
			x	2040.0
			y	829.221923828125
		]
		LabelGraphics
		[
			text	"tinyint"
			fontSize	10
			fontName	"Helvetica"
			x	2209.0
			y	829.221923828125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"prefers_extra_towels"
			fontSize	10
			fontName	"Helvetica"
			x	2040.0
			y	847.472900390625
		]
		LabelGraphics
		[
			text	"tinyint"
			fontSize	10
			fontName	"Helvetica"
			x	2209.0
			y	847.472900390625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"prefers_baby_crib"
			fontSize	10
			fontName	"Helvetica"
			x	2040.0
			y	865.723876953125
		]
		LabelGraphics
		[
			text	"tinyint"
			fontSize	10
			fontName	"Helvetica"
			x	2209.0
			y	865.723876953125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"guest_language"
			fontSize	10
			fontName	"Helvetica"
			x	2040.0
			y	883.974853515625
		]
		LabelGraphics
		[
			text	"int"
			fontSize	10
			fontName	"Helvetica"
			x	2209.0
			y	883.974853515625
		]
	]
	node
	[
		id	17
		graphics
		[
			x	2850.0
			y	650.0
			w	262.0
			h	102.93017578125
			customconfiguration	"DEFAULT"
			fill	"#FFFFCC"
			outline	"#808080"
		]
		LabelGraphics
		[
			text	"api_signers_map_user_reward"
			fontSize	13
			fontStyle	"bold"
			fontName	"Dialog"
			anchor	"t"
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	2743.0
			y	628.461181640625
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	2890.0
			y	628.461181640625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"api_signers_map_user_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	2743.0
			y	646.712158203125
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	2890.0
			y	646.712158203125
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"api_signers_map_reward_id"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	2743.0
			y	664.963134765625
		]
		LabelGraphics
		[
			text	"int unsigned"
			fontSize	10
			fontName	"Helvetica"
			x	2890.0
			y	664.963134765625
		]
		LabelGraphics
		[
		]
		LabelGraphics
		[
			text	"guest_reward_number"
			fontSize	10
			fontStyle	"bold"
			fontName	"Helvetica"
			x	2743.0
			y	683.214111328125
		]
		LabelGraphics
		[
			text	"varchar(255)"
			fontSize	10
			fontName	"Helvetica"
			x	2890.0
			y	683.214111328125
		]
	]
	edge
	[
		source	1
		target	0
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	800.0
					y	425.0
				]
				point
				[
					x	800.0
					y	241.66666666666669
				]
				point
				[
					x	2850.0
					y	225.0
				]
			]
		]
		edgeAnchor
		[
			ySource	-1.0
			xTarget	-1.0
			yTarget	0.21139444410800934
		]
	]
	edge
	[
		source	1
		target	2
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	800.0
					y	425.0
				]
				point
				[
					x	500.0
					y	425.0
				]
				point
				[
					x	475.0
					y	1025.0
				]
			]
		]
		edgeAnchor
		[
			xSource	-1.0
			xTarget	0.21645021438598633
			yTarget	-1.0
		]
	]
	edge
	[
		source	3
		target	0
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	1125.0
					y	525.0
				]
				point
				[
					x	1125.0
					y	250.0
				]
				point
				[
					x	2850.0
					y	225.0
				]
			]
		]
		edgeAnchor
		[
			ySource	-1.0
			xTarget	-1.0
			yTarget	0.3170916736125946
		]
	]
	edge
	[
		source	3
		target	2
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	1125.0
					y	525.0
				]
				point
				[
					x	525.0
					y	525.0
				]
				point
				[
					x	475.0
					y	1025.0
				]
			]
		]
		edgeAnchor
		[
			xSource	-1.0
			xTarget	0.43290042877197266
			yTarget	-1.0
		]
	]
	edge
	[
		source	4
		target	3
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			xSource	-1.0
			xTarget	1.0
		]
	]
	edge
	[
		source	5
		target	0
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	1800.0
					y	725.0
				]
				point
				[
					x	1800.0
					y	258.3333333333333
				]
				point
				[
					x	2850.0
					y	225.0
				]
			]
		]
		edgeAnchor
		[
			ySource	-1.0
			xTarget	-1.0
			yTarget	0.4227888882160187
		]
	]
	edge
	[
		source	5
		target	2
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	1800.0
					y	725.0
				]
				point
				[
					x	550.0
					y	725.0
				]
				point
				[
					x	475.0
					y	1025.0
				]
			]
		]
		edgeAnchor
		[
			xSource	-1.0
			xTarget	0.649350643157959
			yTarget	-1.0
		]
	]
	edge
	[
		source	6
		target	0
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			xSource	1.0
			xTarget	-1.0
		]
	]
	edge
	[
		source	7
		target	0
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			ySource	1.0
			yTarget	-1.0
		]
	]
	edge
	[
		source	8
		target	0
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	1975.0
					y	1025.0
				]
				point
				[
					x	1975.0
					y	266.6666666666667
				]
				point
				[
					x	2850.0
					y	225.0
				]
			]
		]
		edgeAnchor
		[
			ySource	-1.0
			xTarget	-1.0
			yTarget	0.5284861326217651
		]
	]
	edge
	[
		source	8
		target	2
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			xSource	-1.0
			xTarget	1.0
		]
	]
	edge
	[
		source	9
		target	0
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			xSource	-1.0
			xTarget	1.0
		]
	]
	edge
	[
		source	2
		target	0
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	475.0
					y	1025.0
				]
				point
				[
					x	475.0
					y	233.33333333333331
				]
				point
				[
					x	2850.0
					y	225.0
				]
			]
		]
		edgeAnchor
		[
			ySource	-1.0
			xTarget	-1.0
			yTarget	0.10569722205400467
		]
	]
	edge
	[
		source	10
		target	2
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			ySource	-1.0
			yTarget	1.0
		]
	]
	edge
	[
		source	10
		target	11
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			xSource	1.0
			xTarget	-1.0
		]
	]
	edge
	[
		source	11
		target	2
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	850.0
					y	1275.0
				]
				point
				[
					x	850.0
					y	1050.0
				]
				point
				[
					x	475.0
					y	1025.0
				]
			]
		]
		edgeAnchor
		[
			ySource	-1.0
			xTarget	1.0
			yTarget	0.21674388647079468
		]
	]
	edge
	[
		source	12
		target	2
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	125.0
					y	1450.0
				]
				point
				[
					x	125.0
					y	1025.0
				]
				point
				[
					x	475.0
					y	1025.0
				]
			]
		]
		edgeAnchor
		[
			ySource	-1.0
			xTarget	-1.0
		]
	]
	edge
	[
		source	13
		target	11
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			ySource	-1.0
			yTarget	1.0
		]
	]
	edge
	[
		source	13
		target	12
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			xSource	-1.0
			xTarget	1.0
		]
	]
	edge
	[
		source	14
		target	0
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			ySource	-1.0
			yTarget	1.0
		]
	]
	edge
	[
		source	15
		target	0
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
			Line
			[
				point
				[
					x	2500.0
					y	650.0
				]
				point
				[
					x	2500.0
					y	275.0
				]
				point
				[
					x	2850.0
					y	225.0
				]
			]
		]
		edgeAnchor
		[
			ySource	-1.0
			xTarget	-1.0
			yTarget	0.6341833472251892
		]
	]
	edge
	[
		source	15
		target	16
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			xSource	-1.0
			xTarget	1.0
		]
	]
	edge
	[
		source	17
		target	14
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			ySource	-1.0
			yTarget	1.0
		]
	]
	edge
	[
		source	17
		target	15
		graphics
		[
			fill	"#808080"
			targetArrow	"standard"
		]
		edgeAnchor
		[
			xSource	-1.0
			xTarget	1.0
		]
	]
]
