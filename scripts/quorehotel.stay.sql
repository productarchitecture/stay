DROP TABLE IF EXISTS quorehotel.stay;

CREATE TABLE quorehotel.stay (
	id int(11) UNSIGNED NOT NULL AUTO_INCREMENT
		COMMENT '{ "alias": "stayId", "comment": "Quore Stay Id" }',
	quoreapi_api_signers_id int(11) NOT NULL
		COMMENT '{ "alias": "integratorId", "comment": "API Signer (integrator) Id" }',
	property_id int(11) NOT NULL
		COMMENT '{ "alias": "propertyId", "comment": "Property Id" }',
	reservation_id varchar(255) DEFAULT NULL
		COMMENT '{ "alias": "reservationId", "comment": "PMS Resevation Id" }',
	booking_confirmation_id varchar(255) DEFAULT NULL
		COMMENT '{ "alias": "bookingConfirmationId", "comment": "PMS Booking Id" }',
	stay_group_id varchar(255) DEFAULT NULL
		COMMENT '{ "alias": "groupId", "comment": "When Stay is part of a Group" }',
	reference_number varchar(255) DEFAULT NULL
		COMMENT '{ "alias": "referenceNumber", "comment": "PMS Booking,Stay Reference Id,Number" }',
	posted_on datetime DEFAULT CURRENT_TIMESTAMP
		COMMENT '{ "alias": "postedOn", "comment": "Date of Creation of Stay, Reservation, Booking" }',
	cancelled_on datetime DEFAULT NULL
		COMMENT '{ "alias": "cancelledOn", "comment": "Date of Cancelation of Stay, Reservation, Booking" }',
	cancellation_number varchar(255) DEFAULT NULL
		COMMENT '{ "alias": "cancellationId", "comment": "PMS Cancellation Number" }',
	last_updated_on datetime DEFAULT NULL
		COMMENT '{ "alias": "lastUpdate", "comment": "Last Update" }',
	comments text DEFAULT NULL
		COMMENT '{ "alias": "comments", "comment": "Any Comments from PMS or Quore" }',
	PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 1,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = '{ "comment": "Bookings or Reservations from Integrator", "constrain": [ 
{ "name": "fk.stay.property", "foreign_key": "property_id", "reference": "quorehotel.property (id)" }, 
{ "name": "fk.stay.quoreapi.api_signers", "foreign_key": "quoreapi_api_signers_id", "reference": "quoreapi.api_signers (id)" } ] }';

ALTER TABLE quorehotel.stay
ADD INDEX `idx.stay.quoreapi_api_signers_id` (quoreapi_api_signers_id),
ADD INDEX `idx.stay.property_id` (property_id),
ADD INDEX `idx.stay.booking_confirmation_id` (booking_confirmation_id),
ADD INDEX `idx.stay.cancellation_number` (cancellation_number),
ADD INDEX `idx.stay.reference_number` (reference_number),
ADD INDEX `idx.stay.reservation_id` (reservation_id),
ADD INDEX `idx.stay.stay_group_id` (stay_group_id);

/*
ALTER TABLE quorehotel.stay
ADD CONSTRAINT `fk.stay.property` FOREIGN KEY (property_id) REFERENCES quorehotel.property (id),
ADD CONSTRAINT `fk.stay.quoreapi.api_signers` FOREIGN KEY (quoreapi_api_signers_id) REFERENCES quoreapi.api_signers (id);
*/

