﻿DROP TABLE IF EXISTS quoreapi.api_signers_map_reward;

CREATE TABLE quoreapi.api_signers_map_reward (
	id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	api_signers_id int(11) DEFAULT NULL,
	signer_reward_id varchar(255) binary DEFAULT NULL,
	signer_reward_name varchar(255) binary DEFAULT NULL,
	quorehotel_reward_program_id int(11) DEFAULT NULL,
	PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 1,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = '{ "comment": "Integrator Rewards Mapping", "constrain": [ { "name": "fk.api_signers_map_reward.api_signers", "foreign_key": "api_signers_id", "reference": "quoreapi.api_signers (id)" }, { "name": "fk.api_signers_map_reward.quorehotel.reward_program", "foreign_key": "quorehotel_reward_program_id", "reference": "quorehotel.reward_program (id)" } ] }';

ALTER TABLE quoreapi.api_signers_map_reward
ADD INDEX `idx.api_signers_map_reward.api_signers_id.signer_reward_id` (api_signers_id, signer_reward_id);

/*
ALTER TABLE quoreapi.api_signers_map_reward
ADD CONSTRAINT `fk.api_signers_map_reward.api_signers` FOREIGN KEY (api_signers_id) REFERENCES quoreapi.api_signers (id),
ADD CONSTRAINT `fk.api_signers_map_reward.quorehotel.reward_program` FOREIGN KEY (quorehotel_reward_program_id) REFERENCES quorehotel.reward_program (id);
*/
