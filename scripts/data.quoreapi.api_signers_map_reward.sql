﻿SET NAMES 'utf8';

INSERT INTO quoreapi.api_signers_map_reward(id, api_signers_id, signer_reward_id, signer_reward_name, quorehotel_reward_program_id) VALUES
(1, 200, '7', 'Award1445', 31)
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_reward_id=VALUES(signer_reward_id), signer_reward_name=VALUES(signer_reward_name), quorehotel_reward_program_id=VALUES(quorehotel_reward_program_id);

INSERT INTO quoreapi.api_signers_map_reward(id, api_signers_id, signer_reward_id, signer_reward_name, quorehotel_reward_program_id) VALUES
(2, 200, '67', 'Global Hotel Alliance', 21)
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_reward_id=VALUES(signer_reward_id), signer_reward_name=VALUES(signer_reward_name), quorehotel_reward_program_id=VALUES(quorehotel_reward_program_id);

INSERT INTO quoreapi.api_signers_map_reward(id, api_signers_id, signer_reward_id, signer_reward_name, quorehotel_reward_program_id) VALUES
(3, 200, '66', 'Siempre Estelar', 14)
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_reward_id=VALUES(signer_reward_id), signer_reward_name=VALUES(signer_reward_name), quorehotel_reward_program_id=VALUES(quorehotel_reward_program_id);
