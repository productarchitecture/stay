﻿SET NAMES 'utf8';

INSERT INTO quorehotel.area_status_stay(id, name, is_occupied, active) VALUES
('IN', 'Checked In', 1, 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), is_occupied=VALUES(is_occupied), active=VALUES(active);

INSERT INTO quorehotel.area_status_stay(id, name, is_occupied, active) VALUES
('INH', 'In House', 0, 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), is_occupied=VALUES(is_occupied), active=VALUES(active);

INSERT INTO quorehotel.area_status_stay(id, name, is_occupied, active) VALUES
('LST', 'Waiting List', 0, 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), is_occupied=VALUES(is_occupied), active=VALUES(active);

INSERT INTO quorehotel.area_status_stay(id, name, is_occupied, active) VALUES
('NSH', 'No Show', 0, 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), is_occupied=VALUES(is_occupied), active=VALUES(active);

INSERT INTO quorehotel.area_status_stay(id, name, is_occupied, active) VALUES
('OUT', 'Checked Out', 0, 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), is_occupied=VALUES(is_occupied), active=VALUES(active);

INSERT INTO quorehotel.area_status_stay(id, name, is_occupied, active) VALUES
('PRE', 'Pre Check-In', 0, 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), is_occupied=VALUES(is_occupied), active=VALUES(active);

INSERT INTO quorehotel.area_status_stay(id, name, is_occupied, active) VALUES
('REQ', 'Requested', 0, 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), is_occupied=VALUES(is_occupied), active=VALUES(active);

INSERT INTO quorehotel.area_status_stay(id, name, is_occupied, active) VALUES
('RES', 'Reserved', 0, 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), is_occupied=VALUES(is_occupied), active=VALUES(active);

INSERT INTO quorehotel.area_status_stay(id, name, is_occupied, active) VALUES
('TRF', 'Transfered', 0, 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), is_occupied=VALUES(is_occupied), active=VALUES(active);

INSERT INTO quorehotel.area_status_stay(id, name, is_occupied, active) VALUES
('UNK', 'Unknown', 0, 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), is_occupied=VALUES(is_occupied), active=VALUES(active);

INSERT INTO quorehotel.area_status_stay(id, name, is_occupied, active) VALUES
('X', 'Cancelled', 0, 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), is_occupied=VALUES(is_occupied), active=VALUES(active);