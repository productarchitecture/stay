﻿DROP TABLE IF EXISTS quoreapi.api_signers_map_user;

CREATE TABLE quoreapi.api_signers_map_user (
	id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	api_signers_id int(11) NOT NULL,
	signer_guest_id varchar(255) DEFAULT NULL,
	signer_guest_first_name varchar(255) DEFAULT NULL,
	signer_guest_last_name varchar(255) DEFAULT NULL,
	signer_guest_mobile_phone varchar(100) DEFAULT NULL,
	signer_guest_email varchar(255) DEFAULT NULL,
	last_update datetime DEFAULT CURRENT_TIMESTAMP,
	user_id int(11) NOT NULL,
	PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 1,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = '{ "comment": "Integrator Guest Mapping", "constrain": [ { "name": "fk.api_signers_map_user.api_signers", "foreign_key": "api_signers_id", "reference": "quoreapi.api_signers (id)" }, { "name": "fk.api_signers_map_user.user", "foreign_key": "user_id", "reference": "quoreapi.user (id)" } ] }';

ALTER TABLE quoreapi.api_signers_map_user
ADD INDEX `idx.api_signers_map_user.api_signers_id.signer_guest_id` (api_signers_id, signer_guest_id);

/*
ALTER TABLE quoreapi.api_signers_map_user
ADD CONSTRAINT `fk.api_signers_map_user.api_signers` FOREIGN KEY (api_signers_id) REFERENCES quoreapi.api_signers (id),
ADD CONSTRAINT `fk.api_signers_map_user.user` FOREIGN KEY (user_id) REFERENCES quoreapi.user (id);
*/
