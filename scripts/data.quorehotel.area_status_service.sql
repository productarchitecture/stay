﻿SET NAMES 'utf8';

INSERT INTO quorehotel.area_status_service(id, name, active) VALUES
('OK', 'In Service', 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), active=VALUES(active);

INSERT INTO quorehotel.area_status_service(id, name, active) VALUES
('OOI', 'Out Of Inventory', 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), active=VALUES(active);

INSERT INTO quorehotel.area_status_service(id, name, active) VALUES
('OOO', 'Out Of Order', 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), active=VALUES(active);

INSERT INTO quorehotel.area_status_service(id, name, active) VALUES
('OOS', 'Out Of Service', 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), active=VALUES(active);
