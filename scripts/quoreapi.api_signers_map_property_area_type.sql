﻿DROP TABLE IF EXISTS quoreapi.api_signers_map_property_area_type;

CREATE TABLE quoreapi.api_signers_map_property_area_type (
	id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	api_signers_map_property_id int(11) UNSIGNED DEFAULT NULL,
	signer_room_type_id varchar(50) NOT NULL,
	signer_room_type_code varchar(50) DEFAULT NULL,
	signer_room_type_name varchar(50) DEFAULT NULL,
	quorehotel_area_alt_name_id int(10) UNSIGNED DEFAULT NULL,
	PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 1,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = '{ "comment": "Integrator Properties Areas Types Mapping", "constrain": [ { "name": "fk.api_signers_map_property_area_type.api_signers_map_property", "foreign_key": "api_signers_map_property_id", "reference": "quoreapi.api_signers_map_property (id)" }, { "name": "fk.api_signers_map_property_area_type.quorehotel.area_alt_name", "foreign_key": "quorehotel_area_alt_name_id", "reference": "quorehotel.area_alt_name (id)" } ] }';

ALTER TABLE quoreapi.api_signers_map_property_area_type
ADD INDEX `idx.api_signers_map_property_area_type.property.area_alt_name_id` (api_signers_map_property_id, quorehotel_area_alt_name_id),
ADD INDEX `idx.api_signers_map_property_area_type.property.room_type_id` (api_signers_map_property_id, signer_room_type_id),
ADD INDEX `idx.api_signers_map_property_area_type.property.room_type_code` (api_signers_map_property_id, signer_room_type_code),
ADD INDEX `idx.api_signers_map_property_area_type.property.room_type_name` (api_signers_map_property_id, signer_room_type_name);

/*
ALTER TABLE quoreapi.api_signers_map_property_area_type
ADD CONSTRAINT `fk.api_signers_map_property_area_type.api_signers_map_property` FOREIGN KEY (api_signers_map_property_id) REFERENCES quoreapi.api_signers_map_property (id),
ADD CONSTRAINT `fk.api_signers_map_property_area_type.quorehotel.area_alt_name` FOREIGN KEY (quorehotel_area_alt_name_id) REFERENCES quorehotel.area_alt_name (id);
*/
