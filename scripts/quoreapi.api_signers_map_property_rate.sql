﻿DROP TABLE IF EXISTS quoreapi.api_signers_map_property_rate;

CREATE TABLE quoreapi.api_signers_map_property_rate (
	id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	api_signers_map_property_id int(11) UNSIGNED NOT NULL,
	signer_rate_id varchar(25) NOT NULL,
	signer_rate_code varchar(25) DEFAULT NULL,
	start_date date DEFAULT NULL,
	end_date date DEFAULT NULL,
	is_active tinyint(1) UNSIGNED DEFAULT 1,
	quorehotel_currency_code_id int(11) DEFAULT NULL,
	PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 1,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = '{ "comment": "Integrator Properties Rates", "constrain": [ { "name": "fk.api_signers_map_property_rate.api_signers_map_property", "foreign_key": "api_signers_map_property_id", "reference": "quoreapi.api_signers_map_property (id)" }, { "name": "fk.api_signers_map_property_rate.quorehotel_currency_code", "foreign_key": "quorehotel_currency_code_id", "reference": "quorehotel.currency_codes (id)" } ] }';

ALTER TABLE quoreapi.api_signers_map_property_rate
ADD INDEX `idx.api_signers_map_property_rate.property_id.rate_id.date` (api_signers_map_property_id, signer_rate_id, end_date);

/*
ALTER TABLE quoreapi.api_signers_map_property_rate
ADD CONSTRAINT `fk.api_signers_map_property_rate.api_signers_map_property` FOREIGN KEY (api_signers_map_property_id) REFERENCES quoreapi.api_signers_map_property (id),
ADD CONSTRAINT `fk.api_signers_map_property_rate.quorehotel_currency_code` FOREIGN KEY (quorehotel_currency_code_id) REFERENCES quorehotel.currency_codes (id);
*/
