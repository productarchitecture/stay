﻿SET NAMES 'utf8';

INSERT INTO quorehotel.area_status_service_reason(id, name, quoreapi_api_signers_id, quoreapi_api_signers_map_property_id, company_id, property_id, active) VALUES
(1, 'In Maintenance', NULL, NULL, NULL, NULL, 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), quoreapi_api_signers_id=VALUES(quoreapi_api_signers_id), quoreapi_api_signers_map_property_id=VALUES(quoreapi_api_signers_map_property_id), company_id=VALUES(company_id), property_id=VALUES(property_id), active=VALUES(active);

INSERT INTO quorehotel.area_status_service_reason(id, name, quoreapi_api_signers_id, quoreapi_api_signers_map_property_id, company_id, property_id, active) VALUES
(2, 'Remodeling', NULL, NULL, NULL, NULL, 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), quoreapi_api_signers_id=VALUES(quoreapi_api_signers_id), quoreapi_api_signers_map_property_id=VALUES(quoreapi_api_signers_map_property_id), company_id=VALUES(company_id), property_id=VALUES(property_id), active=VALUES(active);
