﻿DROP TABLE IF EXISTS quoreapi.api_signers_map_property_area;

CREATE TABLE quoreapi.api_signers_map_property_area (
	id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	api_signers_map_property_id int(11) UNSIGNED DEFAULT NULL,
	signer_room_id varchar(50) NOT NULL,
	signer_room_name varchar(50) DEFAULT NULL,
	quorehotel_area_base_id int(11) UNSIGNED DEFAULT NULL,
	api_signers_map_property_area_type_id int(11) UNSIGNED DEFAULT NULL,
	PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 1,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = '{ "comment": "Integrator Properties Areas Mapping", "constrain": [ { "name": "fk.api_signers_map_property_area.api_signers_map_property", "foreign_key": "api_signers_map_property_id", "reference": "quoreapi.api_signers_map_property (id)" }, { "name": "fk.api_signers_map_property_area.api_signers_map_property_area_t", "foreign_key": "api_signers_map_property_area_type_id", "reference": "quoreapi.api_signers_map_property_area_type (id)" }, { "name": "fk.api_signers_map_property_area.quorehotel.area_base", "foreign_key": "quorehotel_area_base_id", "reference": "quorehotel.area_base (id)" } ] }';

ALTER TABLE quoreapi.api_signers_map_property_area
ADD INDEX `idx.api_signers_map_property_area.property.room_id` (api_signers_map_property_id, signer_room_id),
ADD INDEX `idx.api_signers_map_property_area.property.area_base` (api_signers_map_property_id, quorehotel_area_base_id),
ADD INDEX `idx.api_signers_map_property_area.property.room_name` (api_signers_map_property_id, signer_room_name),
ADD INDEX `idx.api_signers_map_property_area.property.area_type` (api_signers_map_property_id, api_signers_map_property_area_type_id);

/*
ALTER TABLE quoreapi.api_signers_map_property_area
ADD CONSTRAINT `fk.api_signers_map_property_area.api_signers_map_property` FOREIGN KEY (api_signers_map_property_id) REFERENCES quoreapi.api_signers_map_property (id),
ADD CONSTRAINT `fk.api_signers_map_property_area.api_signers_map_property_area_t` FOREIGN KEY (api_signers_map_property_area_type_id) REFERENCES quoreapi.api_signers_map_property_area_type (id),
ADD CONSTRAINT `fk.api_signers_map_property_area.quorehotel.area_base` FOREIGN KEY (quorehotel_area_base_id) REFERENCES quorehotel.area_base (id);
*/
