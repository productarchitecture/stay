﻿SET NAMES 'utf8';

INSERT INTO quorehotel.area_status_cleaning(id, name, active) VALUES
('CLN', 'Clean', 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), active=VALUES(active);

INSERT INTO quorehotel.area_status_cleaning(id, name, active) VALUES
('DRT', 'Dirty', 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), active=VALUES(active);

INSERT INTO quorehotel.area_status_cleaning(id, name, active) VALUES
('INS', 'Inspected', 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), active=VALUES(active);

INSERT INTO quorehotel.area_status_cleaning(id, name, active) VALUES
('PIC', 'Pickup', 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), active=VALUES(active);

INSERT INTO quorehotel.area_status_cleaning(id, name, active) VALUES
('UNK', 'Unknown', 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), name=VALUES(name), active=VALUES(active);