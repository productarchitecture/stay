﻿DROP TABLE IF EXISTS quoreapi.api_signers_map_area_status_cleaning;

CREATE TABLE quoreapi.api_signers_map_area_status_cleaning (
	id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	api_signers_id int(11) NOT NULL,
	signer_area_status_cleaning_code varchar(50) NOT NULL,
	quorehotel_area_status_cleaning_id char(3) NOT NULL,
	quorehotel_area_status_service_id char(3) DEFAULT NULL,
	PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 1,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = '{ "comment": "Integrator Cleaning Statuses Mapping", "constrain": [ { "name": "fk.api_signers_map_area_status_cleaning.api_signers", "foreign_key": "api_signers_id", "reference": "quoreapi.api_signers (id)" }, { "name": "fk.api_signers_map_area_status_cleaning.area_status_cleaning", "foreign_key": "quorehotel_area_status_cleaning_id", "reference": "quorehotel.area_status_cleaning (id)" }, { "name": "fk.api_signers_map_area_status_cleaning.area_status_service", "foreign_key": "quorehotel_area_status_service_id", "reference": "quorehotel.area_status_service (id)" } ] }';

ALTER TABLE quoreapi.api_signers_map_area_status_cleaning
ADD INDEX `idx.api_signers_map_area_status_cleaning.signer.clean_code` (api_signers_id, signer_area_status_cleaning_code);

/*
ALTER TABLE quoreapi.api_signers_map_area_status_cleaning
ADD CONSTRAINT `fk.api_signers_map_area_status_cleaning.api_signers` FOREIGN KEY (api_signers_id) REFERENCES quoreapi.api_signers (id),
ADD CONSTRAINT `fk.api_signers_map_area_status_cleaning.area_status_cleaning` FOREIGN KEY (quorehotel_area_status_cleaning_id) REFERENCES quorehotel.area_status_cleaning (id),
ADD CONSTRAINT `fk.api_signers_map_area_status_cleaning.area_status_service` FOREIGN KEY (quorehotel_area_status_service_id) REFERENCES quorehotel.area_status_service (id);
*/
