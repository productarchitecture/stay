﻿SET NAMES 'utf8';

INSERT INTO quoreapi.api_signers(id, signer_name, active, remarks, allow_use_of_all_areas, guest_name_to_be_displayed, automatic_callbacks) VALUES
(200, 'StayNTouch', True, 'https://www.stayntouch.com/', True, 'Stay N Touch (PMS)', 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), signer_name=VALUES(signer_name), active=VALUES(active), remarks=VALUES(remarks), allow_use_of_all_areas=VALUES(allow_use_of_all_areas), guest_name_to_be_displayed=VALUES(guest_name_to_be_displayed), automatic_callbacks=VALUES(automatic_callbacks);

INSERT INTO quoreapi.api_signers(id, signer_name, active, remarks, allow_use_of_all_areas, guest_name_to_be_displayed, automatic_callbacks) VALUES
(201, 'Hapi', True, 'https://hapicloud.io/', True, 'Hapi Cloud (integrator)', 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), signer_name=VALUES(signer_name), active=VALUES(active), remarks=VALUES(remarks), allow_use_of_all_areas=VALUES(allow_use_of_all_areas), guest_name_to_be_displayed=VALUES(guest_name_to_be_displayed), automatic_callbacks=VALUES(automatic_callbacks);

INSERT INTO quoreapi.api_signers(id, signer_name, active, remarks, allow_use_of_all_areas, guest_name_to_be_displayed, automatic_callbacks) VALUES
(202, 'Impala', True, 'https://getimpala.com/', True, 'Impala (integrator)', 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), signer_name=VALUES(signer_name), active=VALUES(active), remarks=VALUES(remarks), allow_use_of_all_areas=VALUES(allow_use_of_all_areas), guest_name_to_be_displayed=VALUES(guest_name_to_be_displayed), automatic_callbacks=VALUES(automatic_callbacks);

INSERT INTO quoreapi.api_signers(id, signer_name, active, remarks, allow_use_of_all_areas, guest_name_to_be_displayed, automatic_callbacks) VALUES
(203, 'SiteMind', True, 'https://www.siteminder.com/', True, 'SiteMinder (integrator)', 1)
ON DUPLICATE KEY UPDATE id=VALUES(id), signer_name=VALUES(signer_name), active=VALUES(active), remarks=VALUES(remarks), allow_use_of_all_areas=VALUES(allow_use_of_all_areas), guest_name_to_be_displayed=VALUES(guest_name_to_be_displayed), automatic_callbacks=VALUES(automatic_callbacks);