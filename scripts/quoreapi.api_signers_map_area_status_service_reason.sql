﻿DROP TABLE IF EXISTS quoreapi.api_signers_map_area_status_service_reason;

CREATE TABLE quoreapi.api_signers_map_area_status_service_reason (
	id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	api_signers_id int(11) DEFAULT NULL,
	api_signers_map_property_id int(11) UNSIGNED DEFAULT NULL,
	signer_reason_code varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
	signer_reason varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
	quorehotel_area_status_service_reason_id int(11) UNSIGNED NOT NULL,
	PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 1,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = '{ "comment": "Integrator Service Statuses Reasons Mapping", "constrain": [ { "name": "fk.api_signers_map_area_status_service_reason.api_signers", "foreign_key": "api_signers_id", "reference": "quoreapi.api_signers (id)" }, { "name": "fk.api_signers_map_area_status_service_reason.map_property", "foreign_key": "api_signers_map_property_id", "reference": "quoreapi.api_signers_map_property (id)" }, { "name": "fk.api_signers_map_area_status_service_reason.quorehotel.assr", "foreign_key": "quorehotel_area_status_service_reason_id", "reference": "quorehotel.area_status_service_reason (id)" } ] }';

ALTER TABLE quoreapi.api_signers_map_area_status_service_reason
ADD INDEX `idx.api_signers_map_area_status_service_reason.spr` (api_signers_id, api_signers_map_property_id, signer_reason_code);

/*
ALTER TABLE quoreapi.api_signers_map_area_status_service_reason
ADD CONSTRAINT `fk.api_signers_map_area_status_service_reason.api_signers` FOREIGN KEY (api_signers_id) REFERENCES quoreapi.api_signers (id),
ADD CONSTRAINT `fk.api_signers_map_area_status_service_reason.api_signers_map_pr` FOREIGN KEY (api_signers_map_property_id) REFERENCES quoreapi.api_signers_map_property (id),
ADD CONSTRAINT `fk.api_signers_map_area_status_service_reason.quorehotel.assr` FOREIGN KEY (quorehotel_area_status_service_reason_id) REFERENCES quorehotel.area_status_service_reason (id);
*/
