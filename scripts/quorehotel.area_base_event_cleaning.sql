DROP TABLE IF EXISTS quorehotel.area_base_event_cleaning;

CREATE TABLE quorehotel.area_base_event_cleaning (
	id int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '{ "comment": "Area Base Event Id" }',
	area_base_id int(11) UNSIGNED NOT NULL COMMENT '{ "comment": "Area Id" }',
	area_status_cleaning_id char(3) NOT NULL COMMENT '{ "comment": "Area Status Cleaning Id" }',
	posted_on datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '{ "comment": "Date/Time of event (UTC)" }',
	posted_by int(11) DEFAULT NULL COMMENT '{ "comment": "User Id of who has posted event (if user)" }',
	quoreapi_api_signers_id int(11) DEFAULT NULL COMMENT '{ "comment": "Integrator Id of who has posted event (if integrator)" }',
	hk_board_room_id int(10) UNSIGNED DEFAULT NULL COMMENT '{ "comment": "House Keeping Board Id (if changed by Hk Board)" }',
	PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = '{ "comment": "Area Base Event Cleaning Stream", "constrain": [ 
{ "name": "fk.area_base_event_cleaning.area_base", "foreign_key": "area_base_id", "reference": "quorehotel.area_base (id)" }, 
{ "name": "fk.area_base_event_cleaning.area_status_cleaning", "foreign_key": "area_status_cleaning_id", "reference": "quorehotel.area_status_cleaning (id)" },
{ "name": "fk.area_base_event_cleaning.hk_board_room", "foreign_key": "hk_board_room_id", "reference": "quorehotel.hk_board_room (id)" },
{ "name": "fk.area_base_event_cleaning.quoreapi.api_signers", "foreign_key": "quoreapi_api_signers_id", "reference": "quoreapi.api_signers (id)" },
{ "name": "fk.area_base_event_cleaning.user", "foreign_key": "posted_by", "reference": "quorehotel.user (id)" } ] }';

ALTER TABLE quorehotel.area_base_event_cleaning
ADD INDEX `idx.area_base_event_cleaning.area_base.status_cleaning.posted_on` (area_base_id, area_status_cleaning_id, posted_on),
ADD INDEX `idx.area_base_event_cleaning.signers.area_base.status.posted` (quoreapi_api_signers_id, area_base_id, area_status_cleaning_id, posted_on),
ADD INDEX `idx.area_base_event_cleaning.hk_board_room_id` (hk_board_room_id);

/*
ALTER TABLE quorehotel.area_base_event_cleaning
ADD CONSTRAINT `fk.area_base_event_cleaning.area_base` FOREIGN KEY (area_base_id) REFERENCES quorehotel.area_base (id),
ADD CONSTRAINT `fk.area_base_event_cleaning.area_status_cleaning` FOREIGN KEY (area_status_cleaning_id) REFERENCES quorehotel.area_status_cleaning (id),
ADD CONSTRAINT `fk.area_base_event_cleaning.hk_board_room` FOREIGN KEY (hk_board_room_id) REFERENCES quorehotel.hk_board_room (id),
ADD CONSTRAINT `fk.area_base_event_cleaning.quoreapi.api_signers` FOREIGN KEY (quoreapi_api_signers_id) REFERENCES quoreapi.api_signers (id),
ADD CONSTRAINT `fk.area_base_event_cleaning.user` FOREIGN KEY (posted_by) REFERENCES quorehotel.user (id);
*/
