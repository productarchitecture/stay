DROP TABLE IF EXISTS quorehotel.area_status_service_reason;

CREATE TABLE quorehotel.area_status_service_reason (
	id int(11) UNSIGNED NOT NULL AUTO_INCREMENT
		COMMENT '{ "comment": "Area Base Service Reason Id" }',
	name varchar(50) NOT NULL
		COMMENT '{ "comment": "Area Base Service Reason Name" }',
	quoreapi_api_signers_id int(11) DEFAULT NULL
		COMMENT '{ "comment": "Integrator Id (if service originated on Integrator)" }',
	quoreapi_api_signers_map_property_id int(11) UNSIGNED DEFAULT NULL
		COMMENT '{ "comment": "Integrator Map Property Id (if service reason originated on Integrator for a specific property)" }',
	company_id int(11) DEFAULT NULL
		COMMENT '{ "comment": "Company Id (if service reason originated on Company)" }',
	property_id int(11) DEFAULT NULL
		COMMENT '{ "comment": "Property Id (if service reason originated on Property)" }',
	active tinyint(1) UNSIGNED DEFAULT 1
		COMMENT '{ "comment": "If it is been used" }',
	PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 1,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = '{ "comment": "Area Service Status Reasons. When api_signers_id AND api_signers_map_property_id AND company_id AND property_id is null, reason is GLOBAL", "is_taxonomy": true, "constrain": [ 
{ "name": "fk.area_status_service_reason.company", "foreign_key": "company_id", "reference": "quorehotel.company (id)" }, 
{ "name": "fk.area_status_service_reason.quoreap_api_signers", "foreign_key": "quoreapi_api_signers_id", "reference": "quoreapi.api_signers (id)" },
{ "name": "fk.area_status_service_reason.quoreapi_api_signers_map_property", "foreign_key": "quoreapi_api_signers_map_property_id", "reference": "quoreapi.api_signers_map_property (id)" },
{ "name": "fk.area_status_service_reason.property", "foreign_key": "property_id", "reference": "quorehotel.property (id)" } ] }';

ALTER TABLE quorehotel.area_status_service_reason
ADD INDEX `idx.area_status_service_reason.quoreapi_api_signers_id` (quoreapi_api_signers_id),
ADD INDEX `idx.area_status_service_reason.map_property_id` (quoreapi_api_signers_map_property_id),
ADD INDEX `idx.area_status_service_reason.company_id` (company_id),
ADD INDEX `idx.area_status_service_reason.property_id` (property_id);

/*
ALTER TABLE quorehotel.area_status_service_reason
ADD CONSTRAINT `fk.area_status_service_reason.company` FOREIGN KEY (company_id) REFERENCES quorehotel.company (id),
ADD CONSTRAINT `fk.area_status_service_reason.quoreapi_api_signers` FOREIGN KEY (quoreapi_api_signers_id) REFERENCES quoreapi.api_signers (id),
ADD CONSTRAINT `fk.area_status_service_reason.map_property` FOREIGN KEY (quoreapi_api_signers_map_property_id) REFERENCES quoreapi.api_signers_map_property (id),
ADD CONSTRAINT `fk.area_status_service_reason.property` FOREIGN KEY (property_id) REFERENCES quorehotel.property (id);
*/
