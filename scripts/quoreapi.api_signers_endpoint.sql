﻿DROP TABLE IF EXISTS quoreapi.api_signers_endpoint;

CREATE TABLE quoreapi.api_signers_endpoint (
	id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	api_signers_id int(11) NOT NULL,
	api_signers_map_property_id int(11) UNSIGNED DEFAULT NULL 
		COMMENT '{ "comment": "Used when endpoint is specific to a particular property from this signer" }',
	started_on datetime DEFAULT CURRENT_TIMESTAMP,
	ended_on datetime DEFAULT NULL,
	endpoint_verb enum ('GET', 'POST', 'PUT', 'PATCH', 'DELETE') DEFAULT NULL,
	endpoint_url mediumtext DEFAULT NULL,
	endpoint_header text DEFAULT NULL,
	endpoint_body text DEFAULT NULL,
	action varchar(50) DEFAULT NULL,
	version char(25) DEFAULT NULL,
	environment char(100) DEFAULT NULL,
	system_name varchar(50) DEFAULT NULL,
	direction enum ('ToQuore', 'FromQuore') DEFAULT NULL,
	signature varchar(100) DEFAULT NULL 
		COMMENT '{ "comment": "Signature Verification (mainly for webhooks)" }',
	PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 1,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = '{ "comment": "Integrator endpoints (their API repository and Webhook calls)", "constrain": [ { "name": "fk.api_signers_endpoint.api_signers_id", "foreign_key": "api_signers_id", "reference": "quoreapi.api_signers (id)" }, { "name": "fk.api_signers_endpoint.api_signers_map_property", "foreign_key": "api_signers_map_property_id", "reference": "quoreapi.api_signers_map_property (id)" } ] }';

ALTER TABLE quoreapi.api_signers_endpoint
ADD INDEX `idx.api_signers_endpoint.api_signers_map_property_id` (api_signers_map_property_id),
ADD INDEX `idx.api_signers_endpoint.api_signers_id` (api_signers_id),
ADD INDEX `idx.api_signers_endpoint.find_action` (api_signers_id, api_signers_map_property_id, environment, action);

/*
ALTER TABLE quoreapi.api_signers_endpoint
ADD CONSTRAINT `fk.api_signers_endpoint.api_signers_id` FOREIGN KEY (api_signers_id) REFERENCES quoreapi.api_signers (id),
ADD CONSTRAINT `fk.api_signers_endpoint.api_signers_map_property` FOREIGN KEY (api_signers_map_property_id) REFERENCES quoreapi.api_signers_map_property (id);
*/

