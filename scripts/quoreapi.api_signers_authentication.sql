﻿DROP TABLE IF EXISTS quoreapi.api_signers_authentication;

CREATE TABLE quoreapi.api_signers_authentication (
	id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	api_signers_id int(11) DEFAULT NULL,
	api_signers_map_property_id int(11) UNSIGNED DEFAULT NULL 
		COMMENT '{ "comment": "Used when endpoint is specific to a particular property from this signer" }',
	environment varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'development',
	token text DEFAULT NULL,
	last_update datetime DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 1,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = '{ "comment": "Integrator authentication token information", "constrain": [ { "name": "fk.api_signers_authentication.api_signers", "foreign_key": "api_signers_id", "reference": "quoreapi.api_signers (id)" }, { "name": "fk.api_signers_authentication.api_signers_map_property", "foreign_key": "api_signers_map_property_id", "reference": "quoreapi.api_signers_map_property (id)" } ] }';
	
ALTER TABLE quoreapi.api_signers_authentication
ADD INDEX `idx.api_signers_authentication.api_signers_id` (api_signers_id),
ADD INDEX `idx.api_signers_authentication.api_signers_map_property_id` (api_signers_map_property_id);

/*
ALTER TABLE quoreapi.api_signers_authentication
ADD CONSTRAINT `fk.api_signers_authentication.api_signers` FOREIGN KEY (api_signers_id) REFERENCES quoreapi.api_signers (id),
ADD CONSTRAINT `fk.api_signers_authentication.api_signers_map_property` FOREIGN KEY (api_signers_map_property_id) REFERENCES quoreapi.api_signers_map_property (id);
*/
