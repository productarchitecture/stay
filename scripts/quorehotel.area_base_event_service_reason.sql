DROP TABLE IF EXISTS quorehotel.area_base_event_service_reason;

CREATE TABLE quorehotel.area_base_event_service_reason (
	area_base_event_service_id int(11) UNSIGNED NOT NULL,
	area_status_service_reason_id int(11) UNSIGNED NOT NULL,
	PRIMARY KEY (area_base_event_service_id, area_status_service_reason_id)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = '{ "comment": "Area Base Event Service Reasons", "constrain": [ 
{ "name": "fk.area_base_event_service_reason.area_base_event_service", "foreign_key": "area_base_event_service_id", "reference": "quorehotel.area_base_event_service (id)" }, 
{ "name": "fk.area_base_event_service_reason.area_status_service_reason", "foreign_key": "area_status_service_reason_id", "reference": "quorehotel.area_status_service_reason (id)" } ] }';

ALTER TABLE quorehotel.area_base_event_service_reason
ADD INDEX `idx.area_base_event_service_reason.service_id.reason_id` (area_base_event_service_id, area_status_service_reason_id),
ADD INDEX `idx.area_base_event_service_reason.reason_id.service_id` (area_status_service_reason_id, area_base_event_service_id);

/*
ALTER TABLE quorehotel.area_base_event_service_reason
ADD CONSTRAINT `fk.area_base_event_service_reason.area_base_event_service` FOREIGN KEY (area_base_event_service_id) REFERENCES quorehotel.area_base_event_service (id),
ADD CONSTRAINT `fk.area_base_event_service_reason.area_status_service_reason` FOREIGN KEY (area_status_service_reason_id) REFERENCES quorehotel.area_status_service_reason (id);
*/
