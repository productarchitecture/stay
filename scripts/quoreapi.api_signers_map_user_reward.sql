﻿DROP TABLE IF EXISTS quoreapi.api_signers_map_user_reward;

CREATE TABLE quoreapi.api_signers_map_user_reward (
	id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	api_signers_map_user_id int(11) UNSIGNED NOT NULL,
	api_signers_map_reward_id int(11) UNSIGNED NOT NULL,
	guest_reward_number varchar(255) NOT NULL,
	PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 1,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = '{ "comment": "Integrator Guest Rewards Mapping", "constrain": [ { "name": "fk.api_signers_map_user_reward.api_signers_map_reward", "foreign_key": "api_signers_map_reward_id", "reference": "quoreapi.api_signers_map_reward (id)" }, { "name": "fk.api_signers_map_user_reward.api_signers_map_user", "foreign_key": "api_signers_map_user_id", "reference": "quoreapi.api_signers_map_user (id)" } ] }';

ALTER TABLE quoreapi.api_signers_map_user_reward
ADD INDEX `idx.api_signers_map_user_reward.signer_map_user.reward_id` (api_signers_map_user_id, api_signers_map_reward_id),
ADD INDEX `idx.api_signers_map_user_reward.reward_id.reward_number` (api_signers_map_reward_id, guest_reward_number);

/*
ALTER TABLE quoreapi.api_signers_map_user_reward
ADD CONSTRAINT `fk.api_signers_map_user_reward.api_signers_map_reward` FOREIGN KEY (api_signers_map_reward_id) REFERENCES quoreapi.api_signers_map_reward (id),
ADD CONSTRAINT `fk.api_signers_map_user_reward.api_signers_map_user` FOREIGN KEY (api_signers_map_user_id) REFERENCES quoreapi.api_signers_map_user (id);
*/
