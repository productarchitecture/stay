﻿DROP TABLE IF EXISTS quoreapi.api_signers_map_area_status_service;

CREATE TABLE quoreapi.api_signers_map_area_status_service (
	api_signers_id int(11) NOT NULL,
	signer_area_status_service_code varchar(50) NOT NULL,
	quorehotel_area_status_service_id char(3) DEFAULT NULL,
	PRIMARY KEY (api_signers_id, signer_area_status_service_code)
)
ENGINE = INNODB,
AUTO_INCREMENT = 1,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = '{ "comment": "Integrator Service Statuses Mapping", "constrain": [ { "name": "fk.api_signers_map_area_status_service.api_signers", "foreign_key": "api_signers_id", "reference": "quoreapi.api_signers (id)" }, { "name": "fk.api_signers_map_area_status_service.area_status_service", "foreign_key": "quorehotel_area_status_service_id", "reference": "quorehotel.area_status_service (id)" } ] }';

ALTER TABLE quoreapi.api_signers_map_area_status_service
ADD INDEX `idx.api_signers_map_area_status_service.signer_area_status_servi` (signer_area_status_service_code);

/*
ALTER TABLE quoreapi.api_signers_map_area_status_service
ADD CONSTRAINT `fk.api_signers_map_area_status_service.api_signers` FOREIGN KEY (api_signers_id) REFERENCES quoreapi.api_signers (id),
ADD CONSTRAINT `fk.api_signers_map_area_status_service.area_status_service` FOREIGN KEY (quorehotel_area_status_service_id) REFERENCES quorehotel.area_status_service (id);
*/
