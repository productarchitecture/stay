﻿DROP TABLE IF EXISTS quoreapi.api_signers_map_area_status_stay;

CREATE TABLE quoreapi.api_signers_map_area_status_stay (
	id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	api_signers_id int(11) NOT NULL,
	signer_area_status_stay_code varchar(50) NOT NULL,
	quorehotel_area_status_stay_id char(3) NOT NULL,
	PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 1,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = '{ "comment": "Integrator Stay Statuses Mapping", "constrain": [ { "name": "fk.api_signers_map_area_status_stay.api_signers", "foreign_key": "api_signers_id", "reference": "quoreapi.api_signers (id)" }, { "name": "fk.api_signers_map_area_status_stay.area_status_stay", "foreign_key": "quorehotel_area_status_stay_id", "reference": "quorehotel.area_status_stay (id)" } ] }';

ALTER TABLE quoreapi.api_signers_map_area_status_stay
ADD INDEX `idx.api_signers_map_area_status_stay.signer_area_status_stay_cod` (signer_area_status_stay_code);

/*
ALTER TABLE quoreapi.api_signers_map_area_status_stay
ADD CONSTRAINT `fk.api_signers_map_area_status_stay.api_signers` FOREIGN KEY (api_signers_id) REFERENCES quoreapi.api_signers (id),
ADD CONSTRAINT `fk.api_signers_map_area_status_stay.area_status_stay` FOREIGN KEY (quorehotel_area_status_stay_id) REFERENCES quorehotel.area_status_stay (id);
*/
