DROP TABLE IF EXISTS quorehotel.area_base_event_service;

CREATE TABLE quorehotel.area_base_event_service (
	id int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '{ "comment": "Area Base Service Event Id" }',
	area_base_id int(11) UNSIGNED NOT NULL COMMENT '{ "comment": "Area Id" }',
	area_status_service_id char(3) DEFAULT NULL,
	posted_on datetime DEFAULT CURRENT_TIMESTAMP COMMENT '{ "comment": "Date/Time of event (UTC)" }',
	posted_by int(11) DEFAULT NULL COMMENT '{ "comment": "User Id of who has posted event (if user)" }',
	quoreapi_api_signers_id int(11) DEFAULT NULL COMMENT '{ "comment": "Integrator Id of who has posted event (if integrator)" }',
	start_datetime datetime NOT NULL COMMENT '{ "comment": "Start DateTime of Service Event (Local Time)" }',
	end_datetime datetime NOT NULL COMMENT '{ "comment": "End DateTime of Service Event (Local Time)" }',
	workorder_id int(11) DEFAULT NULL COMMENT '{ "comment": "Workorder Id (if service status changed by a workorder)" }',
	insp_inspection_items_id int(11) DEFAULT NULL COMMENT '{ "comment": "Inspection Item Id (if service status changed by a inspection)" }',
	notes text DEFAULT NULL COMMENT '{ "comment": "Notes about Service" }',
	PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = '{ "comment": "Area Base Event Service Stream", "constrain": [ 
{ "name": "fk.area_base_event_service.area_base", "foreign_key": "area_base_id", "reference": "quorehotel.area_base (id)" }, 
{ "name": "fk.area_base_event_service.area_status_service", "foreign_key": "area_status_service_id", "reference": "quorehotel.area_status_cleaning (id)" },
{ "name": "fk.area_base_event_service.insp_inspection_items", "foreign_key": "insp_inspection_items", "reference": "quorehotel.hk_board_room (id)" },
{ "name": "fk.area_base_event_service.quoreapi.api_signers", "foreign_key": "quoreapi_api_signers_id", "reference": "quoreapi.api_signers (id)" },
{ "name": "fk.area_base_event_service.user", "foreign_key": "posted_by", "reference": "quorehotel.user (id)" },
{ "name": "fk.area_base_event_service.workorder", "foreign_key": "workorder_id", "reference": "quorehotel.workorder (id)" } ] }';

ALTER TABLE quorehotel.area_base_event_service
ADD INDEX `idx.area_base_event_service.area_base.status_service.posted` (area_base_id, area_status_service_id, posted_on),
ADD INDEX `idx.area_base_event_service.signers.area_base.status.posted` (quoreapi_api_signers_id, area_base_id, area_status_service_id, posted_on),
ADD INDEX `idx.area_base_event_service.insp_inspection_items` (insp_inspection_items_id),
ADD INDEX `idx.area_base_event_service.workorder_id` (workorder_id);

/*
ALTER TABLE quorehotel.area_base_event_service
ADD CONSTRAINT `fk.area_base_event_service.area_base` FOREIGN KEY (area_base_id) REFERENCES quorehotel.area_base (id),
ADD CONSTRAINT `fk.area_base_event_service.area_status_service` FOREIGN KEY (area_status_service_id) REFERENCES quorehotel.area_status_service (id),
ADD CONSTRAINT `fk.area_base_event_service.insp_inspection_items` FOREIGN KEY (insp_inspection_items_id) REFERENCES quorehotel.insp_inspection_items (id),
ADD CONSTRAINT `fk.area_base_event_service.quoreapi.api_signers` FOREIGN KEY (quoreapi_api_signers_id) REFERENCES quoreapi.api_signers (id),
ADD CONSTRAINT `fk.area_base_event_service.user` FOREIGN KEY (posted_by) REFERENCES quorehotel.user (id),
ADD CONSTRAINT `fk.area_base_event_service.workorder` FOREIGN KEY (workorder_id) REFERENCES quorehotel.workorder (id);
*/
