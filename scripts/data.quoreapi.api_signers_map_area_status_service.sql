﻿SET NAMES 'utf8';
INSERT INTO quoreapi.api_signers_map_area_status_service(api_signers_id, signer_area_status_service_code, quorehotel_area_status_service_id) VALUES
(200, 'IN_SERVICE', 'OK')
ON DUPLICATE KEY UPDATE api_signers_id=VALUES(api_signers_id), signer_area_status_service_code=VALUES(signer_area_status_service_code), quorehotel_area_status_service_id=VALUES(quorehotel_area_status_service_id);

INSERT INTO quoreapi.api_signers_map_area_status_service(api_signers_id, signer_area_status_service_code, quorehotel_area_status_service_id) VALUES
(200, 'OUT_OF_ORDER', 'OOO')
ON DUPLICATE KEY UPDATE api_signers_id=VALUES(api_signers_id), signer_area_status_service_code=VALUES(signer_area_status_service_code), quorehotel_area_status_service_id=VALUES(quorehotel_area_status_service_id);

INSERT INTO quoreapi.api_signers_map_area_status_service(api_signers_id, signer_area_status_service_code, quorehotel_area_status_service_id) VALUES
(200, 'OUT_OF_SERVICE', 'OOS')
ON DUPLICATE KEY UPDATE api_signers_id=VALUES(api_signers_id), signer_area_status_service_code=VALUES(signer_area_status_service_code), quorehotel_area_status_service_id=VALUES(quorehotel_area_status_service_id);