DROP TABLE IF EXISTS quorehotel.stay_room;

CREATE TABLE quorehotel.stay_room (
	id int(11) UNSIGNED NOT NULL AUTO_INCREMENT
		COMMENT '{ "alias": "stayRoomId", "comment": "Stay Room Id" }',
	stay_id int(11) UNSIGNED DEFAULT NULL
		COMMENT '{ "alias": "stayId", "comment": "Stay,Reservation,Booking Id" }',
	area_base_id int(11) UNSIGNED DEFAULT NULL
		COMMENT '{ "alias": "roomId", "comment": "Area Base Id (Room). Room can exist without area_base_id assigned" }',
	room_rate_code varchar(255) DEFAULT NULL
		COMMENT '{ "alias": "rateCode", "comment": "PMS Room Rate Code" }',
	room_block_code varchar(255) DEFAULT NULL
		COMMENT '{ "alias": "blockCode", "comment": "PMS Room Block Code" }',
	posted_on datetime DEFAULT CURRENT_TIMESTAMP
		COMMENT '{ "alias": "postedOn", "comment": "When room was created" }',
	arrival_date_time datetime DEFAULT NULL
		COMMENT '{ "alias": "arrivalDateTime", "comment": "Guests Arrival Date and Time (Local Time)" }',
	departure_date_time datetime DEFAULT NULL
		COMMENT '{ "alias": "departureDateTime", "comment": "Guests Departure Date and Time (Local Time)" }',
	adults int(3) DEFAULT 0
		COMMENT '{ "alias": "numberOfAdults", "comment": "How many adults in room" }',
	children int(3) DEFAULT 0
		COMMENT '{ "alias": "numberOfChildren", "comment": "How many children in room" }',
	infant int(3) DEFAULT 0
		COMMENT '{ "alias": "numberOfInfants", "comment": "How many infants in room" }',
	last_updated_on datetime DEFAULT CURRENT_TIMESTAMP
		COMMENT '{ "alias": "lastUpdate", "comment": "Last updated" }',
	PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 1,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = '{ "comment": "Room(s) assigned to a Stay,Reservation,Booking", "constrain": [ { "name": "fk.stay_room.area_base", "foreign_key": "area_base_id", "reference": "quorehotel.area_base (id)" }, { "name": "fk.stay_room.stay", "foreign_key": "stay_id", "reference": "quorehotel.stay (id)" } ] }';

ALTER TABLE quorehotel.stay_room
ADD INDEX `idx.stay_room.stay_id.area_base_id` (stay_id, area_base_id),
ADD INDEX `idx.stay_room.area_base_id.stay_id` (area_base_id, stay_id);

/*
ALTER TABLE quorehotel.stay_room
ADD CONSTRAINT `fk.stay_room.area_base` FOREIGN KEY (area_base_id) REFERENCES quorehotel.area_base (id),
ADD CONSTRAINT `fk.stay_room.stay` FOREIGN KEY (stay_id) REFERENCES quorehotel.stay (id);
*/
