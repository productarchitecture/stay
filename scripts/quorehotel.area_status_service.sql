DROP TABLE IF EXISTS quorehotel.area_status_service;

CREATE TABLE quorehotel.area_status_service (
	id char(3) NOT NULL 
		COMMENT '{ "comment": "Area Service Status Id (3 letters)" }',
	name varchar(50) NOT NULL 
		COMMENT '{ "comment": "Area Service Status Name" }',
	active tinyint(1) UNSIGNED DEFAULT 0 
		COMMENT '{ "comment": "If Area Service Status it is in use" }',
	PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = '{ "comment": "Area Service Statuses", "is_taxonomy": true }';
