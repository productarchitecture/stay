﻿DROP TABLE IF EXISTS quoreapi.api_signers_entity;

CREATE TABLE quoreapi.api_signers_entity (
	id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	api_signers_id int(11) DEFAULT NULL,
	signer_entity_id varchar(255) DEFAULT NULL,
	type enum ('Company', 'TravelAgency', 'Other') DEFAULT NULL,
	api_signers_map_property_id int(11) UNSIGNED DEFAULT NULL
		COMMENT '{ "comment": "Used when endpoint is specific to a particular property from this signer" }',
	address_line_1 varchar(255) DEFAULT NULL,
	address_line_2 varchar(255) DEFAULT NULL,
	address_line_3 varchar(255) DEFAULT NULL,
	city varchar(255) DEFAULT NULL,
	state varchar(50) DEFAULT NULL,
	postal_code varchar(50) DEFAULT NULL,
	iso_country_code varchar(10) DEFAULT NULL,
	inserted_on datetime DEFAULT CURRENT_TIMESTAMP,
	updated_on datetime DEFAULT NULL,
	deactivated_on datetime DEFAULT NULL,
	PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 1,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci
COMMENT = '{ "comment": "Integrator Entities (Company, Travel Agencies, Others)", "constrain": [ { "name": "fk.api_signers_entity.api_signers", "foreign_key": "api_signers_id", "reference": "quoreapi.api_signers (id)" }, { "name": "fk.api_signers_entity.api_signers_map_property", "foreign_key": "api_signers_map_property_id", "reference": "quoreapi.api_signers_map_property (id)" } ] }';

ALTER TABLE api_signers_entity
ADD INDEX `idx.api_signers_entity.api_signers_id.signer_entity_id` (api_signers_id, signer_entity_id),
ADD INDEX `idx.api_signers_entity.api_signers_id.type` (api_signers_id, type);

/*
ALTER TABLE api_signers_entity
ADD CONSTRAINT `fk.api_signers_entity.api_signers` FOREIGN KEY (api_signers_id) REFERENCES api_signers (id),
ADD CONSTRAINT `fk.api_signers_entity.api_signers_map_property` FOREIGN KEY (api_signers_map_property_id) REFERENCES api_signers_map_property (id);
*/
