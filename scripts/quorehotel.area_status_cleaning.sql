DROP TABLE IF EXISTS quorehotel.area_status_cleaning;

CREATE TABLE quorehotel.area_status_cleaning (
	id char(3) NOT NULL 
		COMMENT '{ "comment": "Area Cleaning Status Id (3 letters)" }',
	name varchar(50) NOT NULL 
		COMMENT '{ "comment": "Area Cleaning Status Name" }',
	active tinyint(1) UNSIGNED DEFAULT 0 
		COMMENT '{ "comment": "If Area Cleaning Status it is in use" }',
	PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = '{ "comment": "Area Cleaning Statuses", "is_taxonomy": true }';
