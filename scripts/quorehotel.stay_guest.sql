DROP TABLE IF EXISTS quorehotel.stay_guest;

CREATE TABLE quorehotel.stay_guest (
	id int(11) UNSIGNED NOT NULL AUTO_INCREMENT
		COMMENT '{ "alias": "stayGuestId", "comment": "Stay Guest Id" }',
	stay_id int(11) UNSIGNED DEFAULT NULL
		COMMENT '{ "alias": "stayId", "comment": "Stay,Reservation,Booking Id. It only happens if stay has no rooms assigned just yet (to register primary guest)" }',
	quoreapi_user_id int(11) DEFAULT NULL
		COMMENT '{ "alias": "guestId", "comment": "API User (Guest) Id" }',
	stay_room_id int(11) UNSIGNED DEFAULT NULL
		COMMENT '{ "alias", "stayRoomId", "comment": "Stay Room Id. It can be null (when room has not been assigned just yet)" }',
	is_main_guest tinyint(1) UNSIGNED DEFAULT 0
		COMMENT '{ "alias", "isMainGuest", "comment": "If this guest is the Main Guest (Stay Owner) on Stay,Reservation,Booking" }',
	posted_on datetime DEFAULT CURRENT_TIMESTAMP
		COMMENT '{ "alias", "postedOn", "comment": "When guest was included in Room" }',
	deleted_on datetime DEFAULT NULL
		COMMENT '{ "alias", "deletedOn", "comment": "When guest was removed from Room" }',
	PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 1,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = '{ "comment": "Guests allocated on Room for a Stay,Reservation,Booking", "constrain": [ { "name": "fk.stay_guest.quoreapi.user", "foreign_key": "quoreapi_user_id", "reference": "quoreapi.user (id)" }, { "name": "fk.stay_guest.stay", "foreign_key": "stay_id", "reference": "quorehotel.stay (id)" }, { "name": "fk.stay_guest.stay_room", "foreign_key": "stay_room_id", "reference": "quorehotel.stay_room (id)" } ] }';

ALTER TABLE quorehotel.stay_guest
ADD INDEX `idx.stay_guest.stay_id` (stay_id),
ADD INDEX `idx.stay_guest.stay_room_id` (stay_room_id),
ADD INDEX `idx.stay_guest.quoreapi_user_id` (quoreapi_user_id);

/*
ALTER TABLE quorehotel.stay_guest
ADD CONSTRAINT `fk.stay_guest.quoreapi.user` FOREIGN KEY (quoreapi_user_id) REFERENCES quoreapi.user (id),
ADD CONSTRAINT `fk.stay_guest.stay` FOREIGN KEY (stay_id) REFERENCES quorehotel.stay (id),
ADD CONSTRAINT `fk.stay_guest.stay_room` FOREIGN KEY (stay_room_id) REFERENCES quorehotel.stay_room (id);
*/

