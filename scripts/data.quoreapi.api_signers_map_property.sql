﻿SET NAMES 'utf8';

INSERT INTO quoreapi.api_signers_map_property(id, api_signers_id, quorehotel_property_id, signer_hotel_id, signer_other_id, signer_hotel_code, signer_hotel_name, signer_hotel_chain, signer_hotel_brand, inserted_on, deactivated_on) VALUES
(1, 200, 42672, '290', NULL, 'AVIDA', 'A Vida', NULL, NULL, '2019-04-15 09:52:39', NULL)
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), quorehotel_property_id=VALUES(quorehotel_property_id), signer_hotel_id=VALUES(signer_hotel_id), signer_other_id=VALUES(signer_other_id), signer_hotel_code=VALUES(signer_hotel_code), signer_hotel_name=VALUES(signer_hotel_name), signer_hotel_chain=VALUES(signer_hotel_chain), signer_hotel_brand=VALUES(signer_hotel_brand), inserted_on=VALUES(inserted_on), deactivated_on=VALUES(deactivated_on);

INSERT INTO quoreapi.api_signers_map_property(id, api_signers_id, quorehotel_property_id, signer_hotel_id, signer_other_id, signer_hotel_code, signer_hotel_name, signer_hotel_chain, signer_hotel_brand, inserted_on, deactivated_on) VALUES
(1, 200, 10271, '315', NULL, 'SURFSAND', 'Surf & Sand Hotel', NULL, NULL, '2019-04-15 09:52:39', NULL)
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), quorehotel_property_id=VALUES(quorehotel_property_id), signer_hotel_id=VALUES(signer_hotel_id), signer_other_id=VALUES(signer_other_id), signer_hotel_code=VALUES(signer_hotel_code), signer_hotel_name=VALUES(signer_hotel_name), signer_hotel_chain=VALUES(signer_hotel_chain), signer_hotel_brand=VALUES(signer_hotel_brand), inserted_on=VALUES(inserted_on), deactivated_on=VALUES(deactivated_on);

INSERT INTO quoreapi.api_signers_map_property(id, api_signers_id, quorehotel_property_id, signer_hotel_id, signer_other_id, signer_hotel_code, signer_hotel_name, signer_hotel_chain, signer_hotel_brand, inserted_on, deactivated_on) VALUES
(1, 200, 42383, '369', 'f5578319-1f0a-4b06-847a-e611b486de47', 'TWA', 'TWA Hotel', NULL, NULL, '2019-04-15 09:52:39', NULL)
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), quorehotel_property_id=VALUES(quorehotel_property_id), signer_hotel_id=VALUES(signer_hotel_id), signer_other_id=VALUES(signer_other_id), signer_hotel_code=VALUES(signer_hotel_code), signer_hotel_name=VALUES(signer_hotel_name), signer_hotel_chain=VALUES(signer_hotel_chain), signer_hotel_brand=VALUES(signer_hotel_brand), inserted_on=VALUES(inserted_on), deactivated_on=VALUES(deactivated_on);
