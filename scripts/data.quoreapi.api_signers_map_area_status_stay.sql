﻿SET NAMES 'utf8';
INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(1, 200, 'CHECKEDOUT', 'OUT')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);

INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(2, 200, 'RESERVED', 'RES')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);

INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(3, 200, 'CHECKEDIN', 'IN')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);

INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(4, 200, 'NOSHOW', 'NSH')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);

INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(5, 200, 'CANCELLED', 'X')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);

INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(6, 200, 'PRE_CHECKIN', 'PRE')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);

INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(7, 201, 'UNKNOWN', 'UNK')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);

INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(8, 201, 'REQUESTED', 'REQ')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);

INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(9, 201, 'RESERVED', 'RES')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);

INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(10, 201, 'CANCELLED', 'X')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);

INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(11, 201, 'IN_HOUSE', 'INH')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);

INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(12, 201, 'CHECKED_OUT', 'OUT')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);

INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(13, 201, 'NO_SHOW', 'NSH')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);

INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(14, 201, 'WAIT_LIST', 'LST')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);

INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(15, 202, 'OTHER', 'UNK')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);

INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(16, 202, 'REQUESTED', 'REQ')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);

INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(17, 202, 'EXPECTED', 'PRE')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);

INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(18, 202, 'CANCELLED', 'X')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);

INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(19, 202, 'CHECKED_IN', 'IN')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);

INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(20, 202, 'CHECKED_OUT', 'OUT')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);

INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(21, 202, 'NO_SHOW', 'NSH')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);

INSERT INTO quoreapi.api_signers_map_area_status_stay(id, api_signers_id, signer_area_status_stay_code, quorehotel_area_status_stay_id) VALUES
(22, 202, 'TRANSFERED', 'TRF')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_stay_code=VALUES(signer_area_status_stay_code), quorehotel_area_status_stay_id=VALUES(quorehotel_area_status_stay_id);
