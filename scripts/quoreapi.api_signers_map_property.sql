﻿DROP TABLE IF EXISTS quoreapi.api_signers_map_property;

CREATE TABLE quoreapi.api_signers_map_property (
	id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	api_signers_id int(11) NOT NULL,
	quorehotel_property_id int(11) DEFAULT NULL,
	signer_hotel_id varchar(255) DEFAULT NULL,
	signer_other_id varchar(255) DEFAULT NULL,
	signer_hotel_code varchar(25) DEFAULT NULL,
	signer_hotel_name varchar(255) DEFAULT NULL,
	signer_hotel_chain varchar(255) DEFAULT NULL,
	signer_hotel_brand varchar(255) DEFAULT NULL,
	inserted_on datetime DEFAULT CURRENT_TIMESTAMP,
	deactivated_on datetime DEFAULT NULL,
	PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 1,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci
COMMENT = '{ "comment": "Integrator Properties Mapping", "constrain": [ { "name": "fk.api_signers_map_property.api_signers", "foreign_key": "api_signers_id", "reference": "quoreapi.api_signers (id)" }, { "name": "fk.api_signers_map_property.quorehotel.property", "foreign_key": "quore_property_id", "reference": "quorehotel.property (id)" } ] }';

ALTER TABLE quoreapi.api_signers_map_property
ADD INDEX `idx.api_signers_map_property.signer.property` (api_signers_id, quorehotel_property_id),
ADD INDEX `idx.api_signers_map_property.signer.chain` (api_signers_id, signer_hotel_chain),
ADD INDEX `idx.api_signers_map_property.signer.code` (api_signers_id, signer_hotel_code),
ADD INDEX `idx.api_signers_map_property.signer.id` (api_signers_id, signer_hotel_id),
ADD INDEX `idx.api_signers_map_property.signer.other` (api_signers_id, signer_other_id);

/*
ALTER TABLE quoreapi.api_signers_map_property
ADD CONSTRAINT `fk.api_signers_map_property.api_signers` FOREIGN KEY (api_signers_id) REFERENCES quoreapi.api_signers (id),
ADD CONSTRAINT `fk.api_signers_map_property.quorehotel.property` FOREIGN KEY (quorehotel_property_id) REFERENCES quorehotel.property (id);
*/
