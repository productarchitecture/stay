﻿DROP TABLE IF EXISTS quoreapi.api_signers_endpoint_operation;

CREATE TABLE quoreapi.api_signers_endpoint_operation (
	id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	api_signers_endpoint_id int(11) UNSIGNED NOT NULL,
	posted_on datetime DEFAULT CURRENT_TIMESTAMP,
	event varchar(100) DEFAULT NULL,
	headers text DEFAULT NULL,
	data longtext DEFAULT NULL,
	post_process_response text DEFAULT NULL
		COMMENT '{ "comment": "Result of post processing (JSON or text)" }',
	post_process_response_date datetime DEFAULT NULL
		COMMENT '{ "comment": "When result of post processing was received (helpfull to calculate how long it took to process operation)" }',
	post_process_successful tinyint(1) UNSIGNED NOT NULL DEFAULT 0
		COMMENT '{ "comment": "Status of post processing (if errors occurred or not)" }',
	PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 1,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = '{ "comment": "Integrator endpoints operations (stream of requests FromQuore or ToQuore with response/result data)", "constrain": [ { "name": "fk.api_signers_endpoint_operation.api_signers_endpoint", "foreign_key": "api_signers_endpoint_id", "reference": "quoreapi.api_signers_endpoint (id)" } ] }';

ALTER TABLE quoreapi.api_signers_endpoint_operation
ADD INDEX `idx.api_signers_endpoint_operation.find_endpoint_events` (api_signers_endpoint_id, event, posted_on);

/*
ALTER TABLE quoreapi.api_signers_endpoint_operation
ADD CONSTRAINT `fk.api_signers_endpoint_operation.api_signers_endpoint` FOREIGN KEY (api_signers_endpoint_id) REFERENCES quoreapi.api_signers_endpoint (id);
*/
