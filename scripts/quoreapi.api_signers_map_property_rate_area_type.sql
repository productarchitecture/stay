﻿DROP TABLE IF EXISTS quoreapi.api_signers_map_property_rate_area_type;

CREATE TABLE quoreapi.api_signers_map_property_rate_area_type (
	api_signers_map_property_rate_id int(11) UNSIGNED NOT NULL,
	api_signers_map_property_area_type_id int(11) UNSIGNED NOT NULL,
	PRIMARY KEY (api_signers_map_property_rate_id, api_signers_map_property_area_type_id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 1,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = '{ "comment": "Integrator Properties Areas Types Rates", "constrain": [ { "name": "fk.api_signers_map_property_rate_area_type.api_signers_map_p_a_t", "foreign_key": "api_signers_map_property_area_type_id", "reference": "quoreapi.api_signers_map_property_area_type (id)" }, { "name": "fk.api_signers_map_property_rate_area_type.api_signers_map_p_r", "foreign_key": "api_signers_map_property_rate_id", "reference": "quoreapi.api_signers_map_property_rate (id)" } ] }';

/*
ALTER TABLE quoreapi.api_signers_map_property_rate_area_type
ADD CONSTRAINT `fk.api_signers_map_property_rate_area_type.api_signers_map_p_a_t` FOREIGN KEY (api_signers_map_property_area_type_id) REFERENCES quoreapi.api_signers_map_property_area_type (id),
ADD CONSTRAINT `fk.api_signers_map_property_rate_area_type.api_signers_map_p_r` FOREIGN KEY (api_signers_map_property_rate_id) REFERENCES quoreapi.api_signers_map_property_rate (id);
*/
