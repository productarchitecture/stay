﻿SET NAMES 'utf8';

INSERT INTO quoreapi.api_signers_map_area_status_cleaning(id, api_signers_id, signer_area_status_cleaning_code, quorehotel_area_status_cleaning_id, quorehotel_area_status_service_id) VALUES
(1, 200, 'CLEAN', 'CLN', NULL)
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_cleaning_code=VALUES(signer_area_status_cleaning_code), quorehotel_area_status_cleaning_id=VALUES(quorehotel_area_status_cleaning_id), quorehotel_area_status_service_id=VALUES(quorehotel_area_status_service_id);

INSERT INTO quoreapi.api_signers_map_area_status_cleaning(id, api_signers_id, signer_area_status_cleaning_code, quorehotel_area_status_cleaning_id, quorehotel_area_status_service_id) VALUES
(2, 200, 'DIRTY', 'DRT', NULL)
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_cleaning_code=VALUES(signer_area_status_cleaning_code), quorehotel_area_status_cleaning_id=VALUES(quorehotel_area_status_cleaning_id), quorehotel_area_status_service_id=VALUES(quorehotel_area_status_service_id);

INSERT INTO quoreapi.api_signers_map_area_status_cleaning(id, api_signers_id, signer_area_status_cleaning_code, quorehotel_area_status_cleaning_id, quorehotel_area_status_service_id) VALUES
(3, 200, 'INSPECTED', 'INS', NULL)
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_cleaning_code=VALUES(signer_area_status_cleaning_code), quorehotel_area_status_cleaning_id=VALUES(quorehotel_area_status_cleaning_id), quorehotel_area_status_service_id=VALUES(quorehotel_area_status_service_id);

INSERT INTO quoreapi.api_signers_map_area_status_cleaning(id, api_signers_id, signer_area_status_cleaning_code, quorehotel_area_status_cleaning_id, quorehotel_area_status_service_id) VALUES
(4, 200, 'PICKUP', 'PIC', NULL)
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_cleaning_code=VALUES(signer_area_status_cleaning_code), quorehotel_area_status_cleaning_id=VALUES(quorehotel_area_status_cleaning_id), quorehotel_area_status_service_id=VALUES(quorehotel_area_status_service_id);

INSERT INTO quoreapi.api_signers_map_area_status_cleaning(id, api_signers_id, signer_area_status_cleaning_code, quorehotel_area_status_cleaning_id, quorehotel_area_status_service_id) VALUES
(5, 202, 'CLEAN', 'CLN', NULL)
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_cleaning_code=VALUES(signer_area_status_cleaning_code), quorehotel_area_status_cleaning_id=VALUES(quorehotel_area_status_cleaning_id), quorehotel_area_status_service_id=VALUES(quorehotel_area_status_service_id);

INSERT INTO quoreapi.api_signers_map_area_status_cleaning(id, api_signers_id, signer_area_status_cleaning_code, quorehotel_area_status_cleaning_id, quorehotel_area_status_service_id) VALUES
(6, 202, 'DIRTY', 'DRT', NULL)
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_cleaning_code=VALUES(signer_area_status_cleaning_code), quorehotel_area_status_cleaning_id=VALUES(quorehotel_area_status_cleaning_id), quorehotel_area_status_service_id=VALUES(quorehotel_area_status_service_id);

INSERT INTO quoreapi.api_signers_map_area_status_cleaning(id, api_signers_id, signer_area_status_cleaning_code, quorehotel_area_status_cleaning_id, quorehotel_area_status_service_id) VALUES
(7, 202, 'INSPECTED', 'INS', NULL)
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_cleaning_code=VALUES(signer_area_status_cleaning_code), quorehotel_area_status_cleaning_id=VALUES(quorehotel_area_status_cleaning_id), quorehotel_area_status_service_id=VALUES(quorehotel_area_status_service_id);

INSERT INTO quoreapi.api_signers_map_area_status_cleaning(id, api_signers_id, signer_area_status_cleaning_code, quorehotel_area_status_cleaning_id, quorehotel_area_status_service_id) VALUES
(8, 202, 'INDETERMINATE', 'UNK', NULL)
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_cleaning_code=VALUES(signer_area_status_cleaning_code), quorehotel_area_status_cleaning_id=VALUES(quorehotel_area_status_cleaning_id), quorehotel_area_status_service_id=VALUES(quorehotel_area_status_service_id);

INSERT INTO quoreapi.api_signers_map_area_status_cleaning(id, api_signers_id, signer_area_status_cleaning_code, quorehotel_area_status_cleaning_id, quorehotel_area_status_service_id) VALUES
(9, 202, 'OTHER', 'UNK', NULL)
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_cleaning_code=VALUES(signer_area_status_cleaning_code), quorehotel_area_status_cleaning_id=VALUES(quorehotel_area_status_cleaning_id), quorehotel_area_status_service_id=VALUES(quorehotel_area_status_service_id);

INSERT INTO quoreapi.api_signers_map_area_status_cleaning(id, api_signers_id, signer_area_status_cleaning_code, quorehotel_area_status_cleaning_id, quorehotel_area_status_service_id) VALUES
(10, 202, 'OUT_OF_SERVICE', 'DRT', 'OOS')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_cleaning_code=VALUES(signer_area_status_cleaning_code), quorehotel_area_status_cleaning_id=VALUES(quorehotel_area_status_cleaning_id), quorehotel_area_status_service_id=VALUES(quorehotel_area_status_service_id);

INSERT INTO quoreapi.api_signers_map_area_status_cleaning(id, api_signers_id, signer_area_status_cleaning_code, quorehotel_area_status_cleaning_id, quorehotel_area_status_service_id) VALUES
(11, 202, 'OUT_OF_ORDER', 'DRT', 'OOO')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_cleaning_code=VALUES(signer_area_status_cleaning_code), quorehotel_area_status_cleaning_id=VALUES(quorehotel_area_status_cleaning_id), quorehotel_area_status_service_id=VALUES(quorehotel_area_status_service_id);

INSERT INTO quoreapi.api_signers_map_area_status_cleaning(id, api_signers_id, signer_area_status_cleaning_code, quorehotel_area_status_cleaning_id, quorehotel_area_status_service_id) VALUES
(12, 202, 'OUT_OF_INVENTORY', 'DRT', 'OOI')
ON DUPLICATE KEY UPDATE id=VALUES(id), api_signers_id=VALUES(api_signers_id), signer_area_status_cleaning_code=VALUES(signer_area_status_cleaning_code), quorehotel_area_status_cleaning_id=VALUES(quorehotel_area_status_cleaning_id), quorehotel_area_status_service_id=VALUES(quorehotel_area_status_service_id);
