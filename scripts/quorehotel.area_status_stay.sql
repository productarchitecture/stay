DROP TABLE IF EXISTS quorehotel.area_status_stay;

CREATE TABLE quorehotel.area_status_stay (
	id char(3) NOT NULL 
		COMMENT '{ "comment": "Area Stay Status Id (3 letters)" }',
	name varchar(50) NOT NULL 
		COMMENT '{ "comment": "Area Stay Status Name" }',
	is_occupied tinyint(1) NOT NULL 
		COMMENT '{ "comment": "If this status means that room is occupied" }',
	active tinyint(1) UNSIGNED DEFAULT 0 
		COMMENT '{ "comment": "If Area Stay Status it is in use" }',
	PRIMARY KEY (id)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = '{ "comment": "Area Stay Statuses", "is_taxonomy": true }';
